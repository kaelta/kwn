import {cleanEnv, str, port} from 'envalid';

function validateEnv(): void {
	cleanEnv(process.env, {
		NODE_ENV: str({
			choices: ['development', 'production'],
		}),
		DATABASE_PASSWORD: str(),
		DATABASE_PATH: str(),
		DATABASE_USERNAME: str(),
		PORT: port({default: 4000}),
	});
}

export default validateEnv;
