import {Router, Request, Response, NextFunction} from 'express';
import Controller from '@/utils/interfaces/controller.interface';
import HttpException from '@/utils/exceptions/http.exception';
import validationMiddleware from '@/middleware/validation.middleware';
import JobService from './job.service';
import validate from './job.validation';

class JobController implements Controller {
	public path = '/jobs';
	public router = Router();
	private JobService = new JobService();

	constructor() {
		this.initialiseRoutes();
	}

	private initialiseRoutes(): void {
        this.router.post(
            `${this.path}`,
            validationMiddleware(validate.create),
            this.create
        );
    }

	private async create (
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response | void> {
        try {
            const {id, body} = request.body;

            const job = await this.JobService.create(id, body);

            response.status(201).json({job});
        } catch (error) {
            next(new HttpException(400, 'Cannot create Job'));
        }
    }
}

export default JobController;
