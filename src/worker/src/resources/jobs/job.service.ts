/* eslint-disable @typescript-eslint/no-explicit-any */

import app from '../../worker';

class JobService {
	public db = app.prisma;
	public async create(ijob: any, instructions: any) {
		try {
			const job = await this.db.job.create(
				{
					select: ijob.id,
					data: instructions.body,
				},
			);

			return job;
		} catch {
			throw new Error('Unable to create job');
		}
	}

	public async delete(job: any) {
		try {
			const del = await this.db.job.findFirstOrThrow(
				{
					select: job.id,
				},
			);

			if (!del || del == null) {
				throw new Error('Job doesn\'t exist!');
			}

			await this.db.job.delete(job);
		} catch (error) {
			throw new Error('Can\'t delete job!' + error);
		}
	}

	public async update(job: any) {
		const njob = await this.db.job.findFirst(job);
		if (!njob) {
			throw new Error('Job does not exist!');
		}

		// Await this.db.job.update({select: njob.id, data: newJob})
	}
}

export default JobService;
