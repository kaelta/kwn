import * as z from 'zod';

const create = z.object({
    id: z.string(),
    body: z.string(),
});

export default { create };