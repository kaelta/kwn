import {Router} from 'express';

class IndexPage {
	public route: Router;

	constructor() {
		this.route = Router();
	}
}

export default IndexPage;
