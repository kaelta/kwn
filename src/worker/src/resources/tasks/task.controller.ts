import {Router, Request, Response, NextFunction} from 'express';
import Controller from '@/utils/interfaces/controller.interface';
import HttpException from '@/utils/exceptions/http.exception';
import validationMiddleware from '@/middleware/validation.middleware';
import TaskService from './task.service';
import validate from './task.validation';

class TaskController implements Controller {
	public path = '/jobs';
	public router = Router();
	private TaskService = new TaskService();

	constructor() {
		this.initialiseRoutes();
	}

	private initialiseRoutes(): void {
        this.router.post(
            `${this.path}`,
            validationMiddleware(validate.create),
            this.create
        );
    }

	private async create (
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<Response | void> {
        try {
            const {id, body} = request.body;

            const task = await this.TaskService.create(id, body);

            response.status(201).json({task});
        } catch (error) {
            next(new HttpException(400, 'Cannot create Task!'));
        }
    }
}

export default TaskController;
