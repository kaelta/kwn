import app from '../../worker';

class TaskService {
	public db = app.prisma;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	public async create(task: any, instructions: any) {
		try {
			const itask = await this.db.job.create(
				{
					select: task.id,
					data: instructions.routineCode,
				},
			);

			return itask;
		} catch {
			throw new Error('Unable to create job');
		}
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	public async delete(job: any) {
		try {
			const del = await this.db.task.findFirstOrThrow(
				{
					select: job.id,
				},
			);

			if (!del || del == null) {
				throw new Error('Job doesn\'t exist!');
			}

			await this.db.task.delete(job);
		} catch (error) {
			throw new Error('Can\'t delete job!' + error);
		}
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	public async update(job: any) {
		const njob = await this.db.job.findFirst(job);
		if (!njob) {
			throw new Error('Job does not exist!');
		}

		// Await this.db.job.update({select: njob.id, data: newJob})
	}
}

export default TaskService;
