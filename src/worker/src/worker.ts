import express, {type Application} from 'express';
import compression from 'compression';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';
import {PrismaClient} from '@prisma/client';
import {Kafka} from 'kafkajs';
import type Controller from '@/utils/interfaces/controller.interface';
import ErrorMiddleware from '@/middleware/error.middleware';

class App {
	public server: Application;
	public port: number;

	// Prisma
	public prisma: PrismaClient;
	static prisma: PrismaClient;

	// Kafka
	public kafka: Kafka;
	static kafka: Kafka;

	constructor(controllers: Controller[], port: number) {
		this.server = express();
		this.port = port;
		this.prisma = new PrismaClient();
		this.kafka = new Kafka({
			clientId: process.env.KAFKA_CLIENTID,
			// eslint-disable-next-line @typescript-eslint/no-var-requires
			brokers: require('node:process').env.KAFKA_BROKERS,
		});

		this.initialiseDatabaseConnection();
		this.initialiseMiddleware();
		this.initialiseControllers(controllers);
		this.initialiseErrorHandling();
	}

	private initialiseMiddleware(): void {
		this.server.use(helmet());
		this.server.use(cors());
		this.server.use(morgan('dev'));
		this.server.use(express.json());
		this.server.use(express.urlencoded({extended: false}));
		this.server.use(compression());
	}

	private initialiseControllers(controllers: Controller[]): void {
		controllers.forEach((controller: Controller) => {
			this.server.use('/api', controller.router);
			console.log(controller.router)
		});
	}

	private initialiseErrorHandling(): void {
		this.server.use(ErrorMiddleware);
	}

	private async initialiseDatabaseConnection(): Promise<void> {
		await this.prisma.$connect();
	}

	public listen(): void {
		this.server.listen(this.port, () => {
			console.log(`Success! (╯ ✧ ▽ ✧ ) ╯ The worker app listening at http://localhost:${this.port}`);
			console.log('-----------------------------------------------------------------------');
			console.log('☆:.｡.o( ≧ ▽ ≦ ) o.｡.:☆');
			console.log('(i made the api very happy im sorry ;///;)');
		});
	}
}

export default App;
