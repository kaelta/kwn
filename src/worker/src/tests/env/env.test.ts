/* eslint-disable @typescript-eslint/no-var-requires */
import test from 'ava';

test('PORT', t => {
	const {PORT} = require('node:process').env;
	if (!PORT) {
		t.fail('No PORT found in .env');
	}

	t.pass();
});

test('NODE_ENV', t => {
	const {NODE_ENV} = require('node:process').env;

	if (!NODE_ENV) {
		t.fail('No NODE_ENV found in .env');
	}

	t.pass();
});

test('DATABASE_PATH', t => {
	const {DATABASE_PATH} = require('node:process').env;

	if (!DATABASE_PATH) {
		t.fail('No DATABASE_PATH found in .env');
	}

	t.pass();
});

test('DATABASE_PASSWORD', t => {
	const {DATABASE_PASSWORD} = require('node:process').env;

	if (!DATABASE_PASSWORD) {
		t.fail('No DATABASE_PASSWORD found in .env');
	}

	t.pass();
});

test('DATABASE_USERNAME', t => {
	const {DATABASE_USERNAME} = require('node:process').env;

	if (!DATABASE_USERNAME) {
		t.fail('No DATABASE_USERNAME found in .env');
	}

	t.pass();
});

test('DATABASE_URL', t => {
	const {DATABASE_URL} = require('node:process').env;

	if (!DATABASE_URL) {
		t.fail('No DATABASE_URL found in .env');
	}

	t.pass();
});
