import test from 'ava';
import {PrismaClient} from '@prisma/client';

const prisma = new PrismaClient();

prisma.$connect();

test('Find Job Queues', async t => {
	try {
		const result = prisma.job.findMany();
		if (!result) {
			t.fail();
		}

		t.pass();
	} finally {
		prisma.$disconnect();
	}
});
