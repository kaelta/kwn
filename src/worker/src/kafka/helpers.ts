function parseString(value?: string | null) {
  return value ? value : undefined
}

function parseStringNumber(value?: string | null) {
  return value ? Number(value) : undefined
}

function parseStringArray(arr?: string[] | null) {
  if (arr && arr.length !== 0) {
    return arr.map((el) => el)
  }
}

function parseNumber(value?: number | null) {
  return value ? Number(value) : undefined
}

export default {
	parseString,
	parseNumber,
	parseStringNumber,
	parseStringArray,
}