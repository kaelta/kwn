import { IContextLabels } from './context-label.type'
import { IReceipt } from './receipt.type'
import { ITransactionContext } from './transaction-context.type'

export interface IResponseValue {
  id: string
  jobUUID: string
  txContext?: ITransactionContext
  receipt?: IReceipt
  errors?: IError[]
  chain?: string
  contextLabels?: IContextLabels
}

interface IError {
  message?: string
  code?: number
  component?: string
}