import {type Request, type Response, type NextFunction, type RequestHandler} from 'express';
import type * as z from 'zod';

function validationMiddleware(schema: z.AnyZodObject | z.ZodOptional<z.AnyZodObject>): RequestHandler {
	return async (
		request: Request,
		response: Response,
		next: NextFunction,
	): Promise<void> => {
		try {
			const introspect = await schema.parseAsync(request.body);
			console.log(introspect)
			next();
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		} catch (error: any) {
			const errors: string[] = [];
			error.details.forEach((error: z.ZodError) => {
				errors.push(error.message);
			});
			response.status(400).send({errors});
		}
	};
}

export default validationMiddleware;
