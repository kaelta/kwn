
import {type Request, type Response, type NextFunction} from 'express';
import type HttpException from '@/utils/exceptions/http.exception';

function errorMiddleware(
	error: HttpException,
	_request: Request,
	response: Response,

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	_next: NextFunction,
): void {
	const status = error.status || 500;
	const message = error.message || 'Something went wrong';
	response.status(status).send({
		status,
		message,
	});
}

export default errorMiddleware;
