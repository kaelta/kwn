import 'dotenv/config.js';
import 'module-alias/register';
import App from './worker';
import validateEnv from '@/utils/validate-env';
import JobController from './resources/jobs/job.controller';
import TaskController from './resources/tasks/task.controller';

export const app = new App(
	[new JobController(), new TaskController()],
	Number(process.env.PORT),
);

(async () => {
	validateEnv();
	app.listen();
})()
	.catch(error => {
		console.error(error.stack);
	})
	.finally(async () => {
		await app.prisma.$disconnect();
	});

