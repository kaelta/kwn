import {builder} from "../builder";

builder.prismaObject('User', {
  fields: (t) => ({
    id: t.exposeID('id'),
    username: t.exposeString('username'),
    email: t.exposeString('email'),
    image: t.exposeString('image', { nullable: true, }),
    role: t.expose('role', { type: Role, }),
    gardens: t.relation('gardens'),
  })
})

const Role = builder.enumType('Role', {
  values: ['USER', 'ADMIN'] as const,
})
