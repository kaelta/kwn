import {builder} from "../builder";

builder.prismaObject('Plant', {
  fields: (t) => ({
    id: t.exposeID('id'),
	name: t.exposeString('name'),
	gardenId: t.exposeID('gardenId', {nullable: true}),
    garden: t.relation('Task'),
	species: t.exposeString('species'),
  })
})

builder.queryField("plants", (t) =>
  t.prismaConnection({
    type: 'Garden',
	cursor: 'id',
    resolve: (query: any) =>
      prisma.garden.findMany({ ...query })
  })
)
