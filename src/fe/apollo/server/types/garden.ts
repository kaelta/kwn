// /graphql/types/Link.ts
import {builder} from "../builder";
import {prisma} from '@/lib/prisma';

builder.prismaObject('Garden', {
  fields: (t) => ({
    id: t.exposeID('id'),
    name: t.exposeString('name'),
    description: t.exposeString('description'),
    userId: t.exposeID('userId', {nullable: true}),
    user: t.relation('User'),
    plants: t.relation('plants'),
  })
})

builder.queryField("gardens", (t) =>
  t.prismaField({
    type: ['Garden'],
    resolve: (query, _parent, _args, _ctx, _info) =>
      prisma.garden.findMany({ ...query })
  })
)

builder.mutationField("Garden", (t) =>
  t.prismaField({
    type: 'Garden',
    args: {
      name: t.arg.string({ required: true }),
      description: t.arg.string({ required: true }),
      plants: t.arg.string({ required: false }),
    },
    resolve: async (query, _parent, args, ctx) => {
      const { name, description } = args

      if (!(await ctx).user) {
        throw new Error("You have to be logged in to perform this action")
      }

      return prisma.garden.create({
        ...query,
        data: {
          name,
          description,
        }
      })
    }
  })
)
