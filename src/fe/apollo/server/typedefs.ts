import "./types/garden";
import "./types/user";
import {builder} from "./builder";

export const schema = builder.toSchema()
