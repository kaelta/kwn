import {ApolloClient, InMemoryCache} from '@apollo/client';

export const apolloClient = new ApolloClient({
	uri: require('process').env.NEXTJS_BASE_URI,
	cache: new InMemoryCache(),
});
