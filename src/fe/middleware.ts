import { auth } from '@/auth'

import { DEFAULT_LOGIN_REDIRECT, apiAuthPrefix, authRoutes, publicRoutes } from './routes'
export default auth((request: any) => {
	const { nextUrl } = request
	const isLoggedIn = !!request.auth
	const isApiAuthRoute = nextUrl.pathname.startsWith(apiAuthPrefix)
	const isPublicRoute = publicRoutes.includes(nextUrl.pathname)
	const isAuthRoute = authRoutes.includes(nextUrl.pathname)

	if (isApiAuthRoute) {
		return null
	}

	if (isAuthRoute) {
		return null
	}

	if (isAuthRoute && isLoggedIn) {
		return Response.redirect(new URL(DEFAULT_LOGIN_REDIRECT, nextUrl))
	}

	if (!isLoggedIn && !isPublicRoute) {
		return Response.redirect(new URL("/auth/login", nextUrl))
	}

	console.log(request)
	return null
})

export const config = {
	matcher: ['/auth/login']
}
