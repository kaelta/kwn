import {atom, useAtom} from 'jotai';
import {atomWithSuspenseQuery} from 'jotai-tanstack-query';
import {gql} from '@apollo/client';

const idAtom = atom(1);
const userAtom = atomWithSuspenseQuery(get => ({
	queryKey: ['users', get(idAtom)],
	async queryFn({queryKey: [, id]}) {
		// What?
	},
}));
