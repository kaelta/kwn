import {atomWithStorage} from 'jotai/utils';

/**
 * Jotai implementation - dark theme
 * ---------------------------------
 * This `atomWithStorage/2` just takes two values to put into *localStorage*
 * in the browser, but not affect the re-render rule of React.
 * @param darkMode {boolean} - true will make the website dark. Shocker.
 * @see https://tutorial.jotai.org/quick-start/persisting-state
 */
export const darkAtom = atomWithStorage('darkMode', false);
