/**
  * Executing raw CQL cypher queries. Use GraphQL instead, seriously.
*/

import {driver} from './client';

/**
  * #read/1, #read/2
  * @param cypher Raw CQL query to execute. Can only take queries and not mutate data.
  * @param [parameters={}] - Any other parameters to attach to the request
  * @link https://neo4j.com/docs/javascript-manual/current/
*/
export async function read(cypher: string, parameters = {}) {
	const session = driver.session();
	try {
		const res = await session.executeRead(tx => tx.run(cypher, parameters));
		const values = res.records.map(record => record.toObject());

		return values;
	} finally {
		await session.close();
	}
}

/**
  * #write/1, #write/2
  * @param cypher Raw CQL query to execute. Can only take mutations and not request data.
  * @param [parameters={}] - Any other parameters to attach to the request
  * @link https://neo4j.com/docs/javascript-manual/current/
*/
export async function write(cypher: string, parameters = {}) {
	const session = driver.session();
	try {
		const res = await session.executeWrite(tx => tx.run(cypher, parameters));
		const values = res.records.map(record => record.toObject());

		return values;
	} finally {
		await session.close();
	}
}
