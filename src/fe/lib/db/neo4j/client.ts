import neo4j from 'neo4j-driver';

const {NEO4J_URI, NEO4J_USERNAME, NEO4J_PASSWORD} = process.env;

export const driver = neo4j.driver(
	NEO4J_URI!,
	neo4j.auth.basic(
		NEO4J_USERNAME!,
		NEO4J_PASSWORD!,
	),
);

const neo4jSession = driver.session();

export async function read(cypher: string, parameters = {}) {
	try {
		const res = await neo4jSession.executeRead(tx => tx.run(cypher, parameters));

		const values = res.records.map(record => record.toObject());

		return values;
	} finally {
		await neo4jSession.close();
	}
}

export async function write(cypher: string, parameters = {}) {
	try {
		const res = await neo4jSession.executeWrite(tx => tx.run(cypher, parameters));

		const values = res.records.map(record => record.toObject());

		return values;
	} finally {
		await neo4jSession.close();
	}
}
