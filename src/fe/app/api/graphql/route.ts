import { ApolloServer } from 'apollo-server-micro';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import 'ts-tiny-invariant'; // Importing this module as a workaround for issue described here: https://github.com/vercel/vercel/discussions/5846
import type { NextApiRequest, NextApiResponse } from 'next';
import {schema} from '@/apollo/server'
import { createContext } from '@/apollo/server/context'

const apolloServer = new ApolloServer({
	context: createContext,
	schema,
	introspection: true,
	plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

const startServer = apolloServer.start();

export default async function GET(request: NextApiRequest, response: NextApiResponse) {
	await startServer;
	await apolloServer.createHandler({
		path: '/api/graphql',
	})(request, response);
}

export const config = {
	api: {
		bodyParser: false,
	},
};
