import { apolloClient } from '@/apollo/client';
import {gql} from '@apollo/client';

export async function POST() {
	const query = gql`
		mutation {
			createUsers(input: [{ username: String, email: String, role: String }]) {
				username
				email
				role
			}
		}
	`;

}
