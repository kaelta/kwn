export async function GET(request: Request) {
	const {name} = await request.json();

	if (!name) {
		return new Response(
			JSON.stringify({
				name: 'Please provide the name of the plant you want',
			}),
		);
	}

	// TODO: Make that fetch the garden!
	return new Response(
		JSON.stringify({}), {status: 200},
	);
}
