'use client';

import { signIn } from 'next-auth/react';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { Button, Link, Input } from '@nextui-org/react';
import { emailSchema, loginUserSchema } from '@/schemas/common';
import type { Email, LoginUserSchema } from '@/schemas/common';

export function WithEmailForm() {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<Email>({
		resolver: zodResolver(emailSchema),
	});


	const onSubmit = async (data: Email) => {
		await signIn('email', { email: data.email, callbackUrl: '/dashboard' });
	};

	return (
		<div className="inline-flex flex-col">
			<form onSubmit={handleSubmit(onSubmit)} className="flex space-x-1">
				<Input
					label="email"
					placeholder="zhora@net.mail"
					{...register('email')}
				/>
				<Button type='submit' className='whitespace-nowrap'>
					Sign in
				</Button>
			</form>
			{errors.email && (
				<p className='text-red-500 text-sm mt-1'>{errors.email.message}</p>
			)}
		</div>
	)
}

export function WithCredentialsForm() {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm<LoginUserSchema>({
		resolver: zodResolver(loginUserSchema),
	});

	const onSubmit = async (data: LoginUserSchema) => {
		await signIn('credentials', {
			email: data.email,
			password: data.password,
			callbackUrl: '/dashboard'
		});
	};

	return (
		<div className='inline-flex flex-col'>
			<form onSubmit={handleSubmit(onSubmit)} className='inline-flex space-x-1 m-auto'>
				<Input
					label="Email"
					placeholder="zhora@net.mail"
					{...register('email', {
						min: 8
					})}
				/>
				<Input
					label="Password"
					type='password'
					{...register('password', {
						min: 8
					})}
				/>
				<Button type='submit' className='whitespace-nowrap'>
					Sign in
				</Button>
			</form>
			{errors.email && (
				<p className='text-red-500 text-sm mt-1'>{errors.email.message}</p>
			)}
			{errors.password && (
				<p className='text-red-500 text-sm mt-1'>{errors.password.message}</p>
			)}
		</div>
	)
}


export default function SignIn() {
	return (
		<div className='inline-flex flex-col gap-4'>
			<WithEmailForm />
			<strong className='text-center'>or</strong>
			<hr />
			<WithCredentialsForm />

			<Button
				variant='solid'
				showAnchorIcon
				as={Link}
				href='/register'
			>
				Or, maybe sign up?
			</Button>
		</div>
	);
}
