import SignIn from './form';
import Logo from '@/components/Logo';

export default function SignInPage() {
	return (
		<main className='h-screen flex items-center justify-center'>
			<div className='flex flex-col items-center space-y-5'>
				<Logo width={156} height={156}/>
				<h1>Welcome to Ceres! Sign in to get started.</h1>
				<SignIn />
				<hr/>
			</div>
		</main>
	);
}
