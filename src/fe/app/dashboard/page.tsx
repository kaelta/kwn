'use client';
import { useUser } from '@auth0/nextjs-auth0/client';
import {
	Card,
	CardHeader,
	CardBody,
	Image,
	Button,
	Spinner,
	Skeleton,
} from '@nextui-org/react';
import {Suspense, useEffect, useState} from 'react';

export default function Dashboard() {
	const [isLoaded, setIsLoaded] = useState(false);
	const {user} = useUser()

	const toggleLoad = () => {
		setIsLoaded(!isLoaded);
	};

	return (
		<div className='flex w-full flex-col gap-4 px-4'>
			<Card className='col-span-12 h-[300px] sm:col-span-4'>
				<CardHeader className='absolute top-1 z-10 flex-col !items-start'>
					<p className='text-tiny font-bold uppercase'>
						Welcome back, {user?.name}
					</p>
					<h4 className='text-large font-medium'>Check your plant health</h4>
				</CardHeader>
				<Image
					removeWrapper
					alt='Card background'
					className='z-0 h-full w-full object-cover'
					src='/images/home-plant.jpeg'
				/>
				<CardBody>
					<Spinner size='lg' />
				</CardBody>
			</Card>
			<div className='space-around inline-flex flex-col gap-4 md:flex-row'>
				<Card className='py-4'>
					<CardHeader className='flex-col items-start px-4 pb-0 pt-2'>
						<p className='text-tiny font-bold uppercase'>Daily Mix</p>
						<small className='text-default-500'>12 Tracks</small>
						<h4 className='text-large font-bold'>Frontend Radio</h4>
					</CardHeader>
					<CardBody className='overflow-visible py-2'>
						<Image
							alt='Card background'
							className='rounded-xl object-cover'
							src='/images/plants-desk.jpeg'
							width={270}
						/>
					</CardBody>
				</Card>
				<Card className='py-4'>
					<CardHeader className='flex-col items-start px-4 pb-0 pt-2'>
						<p className='text-tiny font-bold uppercase'>Daily Mix</p>
						<small className='text-default-500'>12 Tracks</small>
						<h4 className='text-large font-bold'>Frontend Radio</h4>
					</CardHeader>
					<CardBody className='overflow-visible py-2'>
						<Image
							alt='Card background'
							className='rounded-xl object-cover'
							src='/images/cat-plant.jpeg'
							width={270}
						/>
					</CardBody>
				</Card>
			</div>

			<div className='flex flex-col gap-3'>
				<Suspense>
					<Card className='w-[200px] space-y-5 p-4' radius='lg'>
						<Skeleton isLoaded={isLoaded} className='rounded-lg'>
							<div className='bg-secondary h-24 rounded-lg'></div>
						</Skeleton>
						<div className='space-y-3'>
							<Skeleton isLoaded={isLoaded} className='w-3/5 rounded-lg'>
								<div className='bg-secondary h-3 w-full rounded-lg'></div>
							</Skeleton>
							<Skeleton isLoaded={isLoaded} className='w-4/5 rounded-lg'>
								<div className='bg-secondary-300 h-3 w-full rounded-lg'></div>
							</Skeleton>
							<Skeleton isLoaded={isLoaded} className='w-2/5 rounded-lg'>
								<div className='bg-secondary-200 h-3 w-full rounded-lg'></div>
							</Skeleton>
						</div>
					</Card>
					<Button size='sm' variant='flat' color='secondary' onPress={toggleLoad}>
						{isLoaded ? 'Show' : 'Hide'} Skeleton
					</Button>
				</Suspense>
			</div>
		</div>
	);
}
