import React, {useCallback, useEffect, useState} from 'react';

function EditGardenPage(id: number): React.ReactElement {
	const [garden, setGarden] = useState<object | null>(null)
	const query = useCallback(async () => {
		const garden = await prisma.garden.findUnique({
			where: {id},
		})

		return garden
	}, [id])

	useEffect(() => {
		const garden = query()
		setGarden(garden)
	}, [id, query])

	return (
		<>
		</>
	);
};

export default EditGardenPage;
