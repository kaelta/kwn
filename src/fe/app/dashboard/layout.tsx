import '../globals.css';
import {Inter} from 'next/font/google';
import Navbar from '../../components/dashboard/Navbar';
import Sidebar from '@/components/dashboard/Sidebar';

export const metadata = {
	title: 'Dashboard | Ceres App',
	description: 'Welcome back, user <3',
};

export default function RootLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<>
			<Navbar/>
			<main className='flex md:flex-row flex-col-reverse w-full'>
				<Sidebar/>
				{children}
			</main>
		</>
	);
}
