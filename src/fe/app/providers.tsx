'use client';

import { apolloClient } from '@/apollo/client';
import { ApolloProvider } from '@apollo/client';
import { NextUIProvider } from '@nextui-org/react';
import { Analytics } from '@vercel/analytics/react';
import { SpeedInsights } from '@vercel/speed-insights/next';
import { SessionProvider } from 'next-auth/react';
import React from "react";
import { auth } from '@/auth';

export async function Providers({ children }: { children: React.ReactNode }): React.ReactElement {
	const session = await auth();
	return (
		<NextUIProvider>
			<SessionProvider session={session}>
				<ApolloProvider client={apolloClient}>
					{children}
				</ApolloProvider>
			</SessionProvider>
			<SpeedInsights />
			<Analytics />
		</NextUIProvider>
	);
}

export default Providers;
