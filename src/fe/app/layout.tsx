import './globals.css';
import {Inter} from 'next/font/google';
import Providers from './providers';
import React from "react";

const inter = Inter({subsets: ['latin']});

export const metadata = {
	title: 'Ceres App',
	description: 'Automated IoT plant caretaking.',
};

export default function RootLayout({
	children,
}: {
	children: React.ReactNode;
}) {
	return (
		<html lang='en' className={inter.className}>
			<body>
				<Providers>
					{children}
				</Providers>
			</body>
		</html>
	);
}
