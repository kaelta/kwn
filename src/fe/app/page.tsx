'use client';

import React from 'react';
import Navbar from '@/components/landing/Navbar';
import { Button, Link } from "@nextui-org/react";
import { useSession } from 'next-auth/react';
export default function Home() {
	const { data: user } = useSession()
	return (
		<React.Fragment>
			<Navbar />
			<main className={'flex flex-col gap-4 justify-center items-center mt-16'}>
				<div id={'copy-header'}>
					<h1 className="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
						Your new favourite <span
							className="underline underline-offset-3 decoration-8 decoration-blue-400 dark:decoration-blue-600">
							monitoring application.</span>
					</h1>
					<p className="text-lg font-normal text-gray-500 lg:text-xl dark:text-gray-400">Ceres is the
						world&apos;s best
						monitoring and logging tool, providing personalised AI powered insights, built for the needs of
						the modern age.</p>
				</div>
				<div className={'flex flex-col md:flex-row gap-4'}>
					<div className={'p-8 text-center gap-8 inline-flex flex-col'}>
						{user ? (
							<>
								<p> But you already know that. Why not <a className={'underline'} href={'/dashboard'}>check in? </a>
								</p>
							</>
						) : (
							<>
								<p>Sound interesting?</p>
								<Button as={Link} color={"primary"} href={'/api/auth/login'}>Login today to get started~</Button>
							</>
						)}
					</div>
				</div>
			</main>
		</React.Fragment>
	)
}
