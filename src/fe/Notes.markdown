# Notes during the development of Ceres frontend

These notes are solely meant for Kae to view at any given point and remember what they were doing at a quick overview.
Don't bother trying to understand this.

## M12282023

It took me too long to realise `next-auth` was a horseshit library, so I'm using Auth0 instead.
To make sure it runs just fine, run:

```bash
ngrok http 3000
```

Another change is using `pothos` to sync graphql and prisma schema types.
We'll migrate production data to Neo4j rather than store it there directly, just because it's easier to work with.

## M12042023

We're using [graphql-code-generator](<https://github.com/dotansimha/graphql-code-generator#readme>) to keep our schemas consistent between frontend and API in the project.
Only problem right now is like, how tf does that work??

So it created like a `codegen.ts` file which I assume is just the config for this program to run, and we're supposed to just run `pnpm codegen` to get it working.
But it says that `DateTime` is an invalid GQL type??? Wtf is this trash.
