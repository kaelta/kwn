import * as z from 'zod';

export const emailSchema = z.object({
	email: z.string().email({
		message: 'Please enter a valid email address',
	}),
});
export type Email = z.infer<typeof emailSchema>;

export const passwordSchema = z.string({description: 'Password'}).min(4);
export type Password = z.infer<typeof passwordSchema>;

export const usernameSchema = z.string().regex(
	/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[\w.]+(?<![_.])$/,
	{
		message: 'Username must be between 8 and 20 characters and can only contain letters, numbers, underscores, and periods.',
	},
);
export type Username = z.infer<typeof usernameSchema>;

export const registerUserSchema = z.object({
	email: z.string().email(),
	username: z.string().min(3).max(32).optional(),
	password: z.string().min(4),
	confirmPassword: z.string().min(4),
}).superRefine(({confirmPassword, password}, ctx) => {
	if (confirmPassword !== password) {
		ctx.addIssue({
			code: 'custom',
			message: 'The passwords did not match',
		});
	}
});
export type ReigsterUserSchema = z.infer<typeof registerUserSchema>;

export const loginUserSchema = z.object({
	email: z.string().email(),
	password: z.string().min(4)
})
export type LoginUserSchema = z.infer<typeof loginUserSchema>;
