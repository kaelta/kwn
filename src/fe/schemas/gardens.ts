import * as z from 'zod';

export const name = z.string().min(4).max(48)
export const description = z.string().min(2).max(128)
export const location = z.string().min(4).max(56).optional()

export const addGarden = z.object({
	name,
	description,
	location,
})
export type AddGarden = z.infer<typeof addGarden>;

export const deleteGardenSchema = z.object({
	name
})
