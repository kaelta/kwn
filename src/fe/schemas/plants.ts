import * as z from 'zod';

export const addPlantSchema = z.object({
	name: z.string().max(64),
	servo: z.string(),
	temperature: z.number().transform(arg => Number(arg.toFixed(1))),
});
export type AddPlant = z.infer<typeof addPlantSchema>;
