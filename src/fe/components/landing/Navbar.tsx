'use client';

import React from "react";
import { Navbar, NavbarBrand, NavbarContent, NavbarItem, Link, Button } from "@nextui-org/react";
import Logo from '@/components/Logo'
import { auth } from "@/auth"

export default async function Header() {
	const session = await auth();
	return (
		<Navbar isBordered>
			<NavbarBrand>
				<Logo />
				<p className="font-bold text-inherit">Ceres</p>
			</NavbarBrand>
			<NavbarContent className="hidden sm:flex gap-4" justify="center">
				<NavbarItem>
					<Link color="foreground" href="#">
						Features
					</Link>
				</NavbarItem>
				<NavbarItem>
					<Link href="#" aria-current="page">
						Writeup
					</Link>
				</NavbarItem>
				<NavbarItem>
					<Link color="foreground" href="#">
						Docs
					</Link>
				</NavbarItem>
			</NavbarContent>
			<NavbarContent justify="end">
				{session!.user ? (
					<>
						<Link href={'/dashboard'}> Dashboard </Link>
					</>
				) : (
					<>
						<NavbarItem className="hidden lg:flex">
							<Link href="#">Login</Link>
						</NavbarItem>
						<NavbarItem>
							<Button as={Link} color="primary" href="#" variant="flat">
								Sign Up
							</Button>
						</NavbarItem>
					</>
				)}

			</NavbarContent>
		</Navbar>
	);
}

