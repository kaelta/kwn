'use client';

import React from 'react';
import {Leaf, Gauge} from 'lucide-react';
import {Navbar, NavbarBrand, NavbarContent, NavbarItem, Button, useDisclosure, User, Link} from '@nextui-org/react';
import Logo from '../Logo';
import SignOutButton from '../ui/buttons/sign-out';
import AddPlantModal from '../ui/modals/add-plant-modal';
import { useUser } from '@auth0/nextjs-auth0/client'

function Header() {
	const {isOpen, onOpen, onOpenChange} = useDisclosure();
	const {user} = useUser();

	return (
		<Navbar maxWidth='full'>
			<NavbarContent justify='start'>
				<NavbarBrand>
					<Logo />
					<p className='font-bold text-inherit'>Ceres</p>
				</NavbarBrand>
			</NavbarContent>
      		<NavbarContent justify='end'>
				<NavbarItem className='hidden lg:flex'>
					<Button variant='flat' href='/profile/gardens'> <Gauge/> Garden Monitor </Button>
				</NavbarItem>
        		<NavbarItem className='hidden lg:flex'>
          			<Button variant='flat' onPress={onOpen}>
						<Leaf/>
						Add Plant
					</Button>
        		</NavbarItem>
        		<NavbarItem>
					{user ? (
						<div className='inline-flex gap-2'>
							<User
								name={user.name}
								description={'Welcome back :)'}
								avatarProps={{src: user.picture!}}
							/>
							<SignOutButton/>
						</div>
					) : (
						<Button as={Link} color='primary' variant='flat' href='/api/auth/login'>
							Sign up
						</Button>
					)}
				</NavbarItem>
			</NavbarContent>
			{isOpen && <AddPlantModal isOpen={isOpen} onOpenChange={onOpenChange}/>}
		</Navbar>
	);
}

export default Header;
