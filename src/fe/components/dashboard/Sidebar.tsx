'use client';

import React from 'react';
import {BarChartIcon, GitlabIcon, TwitterIcon} from 'lucide-react';

function Box(props: {className?: string; children: React.ReactNode}) {
	return (
		<div className={`flex flex-col p-4 w-full rounded-lg ${props.className ?? null}`}>
			{props.children}
		</div>
	);
}

function Sidebar() {
	return (
		<aside className='flex flex-row md:flex-col justify-between w-1/3 h-full'>
			<div className='flex md:flex-col gap-2 p-2 my-8 overflow-y-scroll'>
				<Box>
					<div className='inline-flex flex-col gap-4'>
						<ul>
							<h3>§ Gardens</h3>
							<li className='ml-4'>
								<b><h4>VÄXER Gardens</h4></b>
							</li>
						</ul>
						<ul>
							<h3>§ Plants</h3>
							<li className='ml-4'>
								<strong>Singular plant</strong>
								<br />
								<strong>Outdoor plant</strong>
							</li>
						</ul>
					</div>
				</Box>
			</div>
			{/* Sidebar footer */}
			<Box className=''>
				<div className='flex justify-evenly gap-12'>
					<a href='https://gitlab.com/kaelta/kwn'>
						<GitlabIcon />
					</a>
					<a href='https://x.com/kaelta_'>
						<TwitterIcon />
					</a>
					<a href='https://gitlab.com/kaelta/kwn'>
						<BarChartIcon />
					</a>
				</div>
			</Box>
		</aside>
	);
}

export default Sidebar;
