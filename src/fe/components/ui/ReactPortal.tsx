import React, {useState, useLayoutEffect, type ReactNode} from 'react';
import {createPortal} from 'react-dom';

export const defaultReactPortalProps = {
	wrapperId: 'react-portal',
};

export type ReactPortalProps = {
	children: ReactNode;
	wrapperId: string;
} & typeof defaultReactPortalProps;

export function ReactPortal({children, wrapperId}: ReactPortalProps) {
	const [wrapper, setWrapper] = useState<Element | undefined>(undefined);
	useLayoutEffect(() => {
		let element = document.getElementById(wrapperId);
		let created = false;

		if (!element) {
			created = true;
			const wrapper = document.createElement('div');
			wrapper.setAttribute('id', wrapperId);
			document.body.append(wrapper);
			element = wrapper;
		}

		setWrapper(element);
		return () => {
			if (created && element?.parentNode) {
				element.remove();
			}
		};
	}, [wrapperId]);

	// Return null on initial rendering.
	if (wrapper === undefined) {
		return null;
	}

	return createPortal(children, wrapper);
}

ReactPortal.defaultProps = defaultReactPortalProps;
export default ReactPortal;
