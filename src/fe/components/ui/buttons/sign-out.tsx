'use client';

import {Button, Link} from '@nextui-org/react'
import {useUser} from '@auth0/nextjs-auth0/client';

export default function SignOutButton() {
	const { user, error, isLoading } = useUser();

	if (isLoading) return <div>Loading...</div>;
	if (error) return <div>{error.message}</div>;

	return user ? <Button as={Link} href="/api/auth/logout">
		Sign out
	</Button> : null;
}
