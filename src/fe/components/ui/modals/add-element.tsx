import React, {useCallback, useEffect, useState} from 'react';
import {
	Modal,
	ModalContent,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Button,
	Input,
	useDisclosure,
} from '@nextui-org/react';
import {useForm} from 'react-hook-form';
import {zodResolver} from '@hookform/resolvers/zod';
import {gql, useQuery} from '@apollo/client';
import ReactPortal from '../ReactPortal';
import {emailSchema} from '@/schemas/common';
import {type AddPlant} from '@/schemas/plants';
import {apolloClient} from '@/apollo/client';

export type Props = {
	isOpen: boolean;
	onOpenChange: () => void;
	formType: any;
};

export function AddPlant(props: Props) {
	const [backdrop, setBackdrop] = useState('opaque');
	const backdrops = ['opaque', 'blur', 'transparent'];

	const {
		register,
		handleSubmit,
		formState: {errors},
	} = useForm<AddPlant>({
		resolver: zodResolver(emailSchema),
	});

	const fetchAllUsersGardens = useCallback(() => {
	}, []);

	useEffect(() => {}, []);

	const checkIfPlantExists = async (
		plant: string,
		garden: string,
	): Promise<boolean> => {
		const {data} = await apolloClient.query({
			query: gql`
				query AddPlant(input: {
					name: ${plant}
					garden: ${garden}
				}) {
					id
					name
					garden
				}
			`,
		});

		return data !== null || false;
	};

	const onSubmit = async (data: AddPlant) => {};

	return (
		<ReactPortal>
			<Modal isOpen={props.isOpen} onOpenChange={props.onOpenChange}>
				<ModalContent>
					{onClose => (
						<>
							<ModalHeader className='flex flex-col gap-1'>
								Add Plant
							</ModalHeader>
							<ModalBody>
								<form
									onSubmit={handleSubmit(onSubmit)}
									className='inline-flex flex-col gap-4 space-x-1'
								>
									<Input
										type='name'
										label='Plant name'
										{...register('name')}
										placeholder='Sprouty Sapling! ⸜(｡˃ ᵕ ˂ )⸝♡'
									/>
									{errors.name && (
										<p className='text-red-500 text-sm mt-1'>
											{errors.name.message?.toString()}
										</p>
									)}
								</form>
							</ModalBody>
							<ModalFooter>
								<Button
									color='danger'
									variant='light'
									onPress={onClose}
								>
									Close
								</Button>
								<Button
									type='submit'
									variant='solid'
									onPress={onClose}
								>
									Add Record
								</Button>
							</ModalFooter>
						</>
					)}
				</ModalContent>
			</Modal>
		</ReactPortal>
	);
}

export default AddPlant;
