const {fontFamily} = require('tailwindcss/defaultTheme');
const {nextui} = require('@nextui-org/react');
const plugin = require('tailwindcss/plugin')

/** @type {import('tailwindcss').Config} */
module.exports = {
	darkMode: ['class', '[data-theme="dark"]'],
	content: [
		'app/**/*.{ts,tsx}',
		'components/**/*.{ts,tsx}',
		'./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
		'./node_modules/@nextui-org/theme/dist/components/(button|snippet|code|input).js',
	],
	theme: {
		extend: {
			fontFamily: {
				sans: ['var(--font-sans)', ...fontFamily.sans],
			},
			keyframes: {
				'accordion-down': {
					from: {height: 0},
					to: {height: 'var(--radix-accordion-content-height)'},
				},
				'accordion-up': {
					from: {height: 'var(--radix-accordion-content-height)'},
					to: {height: 0},
				},
			},
			animation: {
				'accordion-down': 'accordion-down 0.2s ease-out',
				'accordion-up': 'accordion-up 0.2s ease-out',
			},
		},
	},
	plugins: [
        plugin(function({ addBase, theme }) {
            addBase({
                'h1': { fontSize: theme('fontSize.5xl') },
                'h2': { fontSize: theme('fontSize.3xl') },
                'h3': { fontSize: theme('fontSize.xl') },
            })
        }),
		nextui({
            prefix: 'n',
            addCommonColors: true,
            defaultTheme: "light",
            themes: {}
        }),
	],
};
