'use server';

import * as z from 'zod';
import { loginUserSchema, LoginUserSchema } from '@/schemas/common';
import { signIn } from '@/auth';
import { DEFAULT_LOGIN_REDIRECT } from '@/routes';
import { AuthError } from 'next-auth';
import { getUserByEmail } from '@/data/user';
import { generateVerificationToken } from '@/lib/tokens';

export const login = async (values: z.infer<typeof loginUserSchema>) => {
	const validatedFields = loginUserSchema.safeParse(values);

	if (!validatedFields.success) {
		return { error: 'Invalid parameters' }
	}

	const { email, password } = validatedFields.data

	const existingUser = await getUserByEmail(email)
	if (!existingUser || !existingUser.email || !existingUser.password) {
		return {error: 'Email does not exist!'}
	}

	if (!existingUser.emailVerified) {
		const verificationToken = await generateVerificationToken(existingUser.email)

		return {success: 'Confirmation email is sent!'}
	}

	try {
		await signIn('credentials', {
			email,
			password,
			redirectTo: DEFAULT_LOGIN_REDIRECT,
		})
	} catch (error) {
		if (error instanceof AuthError) {
			switch (error.type) {
				case 'CredentialsSignin': {
					return {error: 'Invalid credentials!'}
				}
				default: {
					return {error: 'sth went wrong lol idk'}
				}
			}
		}

		// Required for us to be redirected~
		throw error;
	}
}
