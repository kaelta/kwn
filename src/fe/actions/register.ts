'use server';

import * as z from 'zod';
import bcrypt from 'bcryptjs';
import { db } from '@/lib/db';
import { registerUserSchema, type ReigsterUserSchema } from '@/schemas/common';
import { getUserByEmail } from '@/data/user';
import { generateVerificationToken } from '@/lib/tokens';

export const register = async (values: z.infer<typeof registerUserSchema>) => {
	const validatedFields = registerUserSchema.safeParse(values);

	if (!validatedFields.success) {
		return {error: 'FUck! bad fields'}
	}

	const { email, password, username } = validatedFields.data;
	const hashedPassword = await bcrypt.hash(password, 10)

	const existingUser = await getUserByEmail(email)

	if (existingUser) {
		return {error: 'User already exists / email is in use.'}
	}

	await db.user.create({
		data: {
			username,
			email,
			password: hashedPassword
		}
	})

	const verificationToken = generateVerificationToken(email)

	return {success: 'Confirmation email sent :)'}
}
