import { PrismaClient } from '@prisma/client'
import { users } from '../data/users'
const prisma = new PrismaClient()

async function main(): Promise<void> {
  await prisma.user.create({
    data: {
		username: `awa`,
		email: `testemail@gmail.com`,
		role: 'ADMIN',
    },
  })

  await prisma.user.createMany({
    data: users,
  })
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
