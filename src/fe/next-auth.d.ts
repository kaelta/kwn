import { Role } from '@prisma/client'
import NextAuth, { type DefaultSession } from 'next-auth'
// If we ever have to assign anything else to our user object that we need
// in the app, we can just add them here :)
export type ExtendedUser = DefaultSession['user'] & {
	role: Role
}
declare module 'next-auth' {
	interface Session {
		user: ExtendedUser
	}
}

