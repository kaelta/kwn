from db import driver

def get_plant_by_id(id):
	records, summary, keys = driver.execute_query(
		"MATCH (p:Plant {id: $id}) RETURN p.id AS id",
		id=42,
		database_="neo4j",
	)

	return records, summary, keys

def match_plant_nodes(name_filter):
    result = driver.execute_query("""
        MATCH (p:Plant) WHERE p.type STARTS WITH $filter
        RETURN p.id AS name ORDER BY id
        """, filter=name_filter)

    return list(result)
