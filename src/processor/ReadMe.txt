# Processor Backend

**This can and will be finished last!!!**

The backend processor shares stats about the plants (ie temperature, pH levels etc)
to & from the hardware controllers via this API.

The idea behind this processor is to process large amounts of data from multiple users to find
relations between plant growth, watering cycle, varying temperatures between the types of plants,
in order to help other users grow their plants more efficiently.

It uses an AI model to generate reports based on trends, and sends these analytics back to the end user via its flask API.
