import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

NEO4J_URI = os.environ.get("NEO4J_URI")
NEO4J_AUTH_USER = os.environ.get("NEO4J_AUTH_USER")
NEO4J_AUTH_PASS = os.environ.get("NEO4J_AUTH_PASS")
