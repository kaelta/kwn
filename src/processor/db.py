from neo4j import GraphDatabase
from config import NEO4J_URI, NEO4J_AUTH_USER, NEO4J_AUTH_PASS

# Must be present at all times!
if not NEO4J_URI:
	raise Exception(ValueError("NEO4J_URI was not found in .env!"))

# Same here, but terse
if not NEO4J_AUTH_PASS or not NEO4J_AUTH_USER:
	raise Exception(ValueError("AUTH vars were not found in .env!"))

URI = NEO4J_URI
AUTH = (NEO4J_AUTH_USER, NEO4J_AUTH_PASS)

with GraphDatabase.driver(URI, auth=AUTH) as driver:
	driver.verify_connectivity()
