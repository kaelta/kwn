<div align="center">
<h1>Ceres</h1>

Studying the effects of music in the growth of plants.
</div>

## What is this?

Ceres is a research project designed to prove the hypothesis of healthier plant growth resulting from listening to different kinds of music.

Following my research on the influence of human emotion through frequencies, I decided to further extend my research upon discovering that a similar theory has also been found in plants[^1].

If the studies prove true, with the maintenance of a consistent control environment (that being, ample light, fresh air, nutrients fed into several plants, etc.) we can observe the changes that occur over time while being subjected to different types of music.
The study will also be conducted on plants that grow outdoors with natural light and further elemental change.

The whitepaper can be found [here](./notes/Whitepaper.odt), and the summary/writeup [here](./notes/Music%20Effect%20on%20Plants.md)

## How does it work?

Ceres' architecture uses:

- **Next.JS** - Frontend
- **Neo4J** - User records, gardens, plants, postables
- **PostgreSQL** - Job queue
- ~~**Go**~~ **Express** - API / Job scheduling and message queue system

You can read more about its arcitecture [here](./notes/Architecture.md)

---

References:
[^1]: <https://www.researchgate.net/publication/291086163_Effect_of_Music_on_Plants_-_An_Overview>
[^2]: <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5797535/>
[^3]: <https://dengarden.com/gardening/the-effect-of-music-on-plant-growth>
