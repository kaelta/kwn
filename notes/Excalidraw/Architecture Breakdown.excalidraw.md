---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Next.JS ^nMwfUyhR

Neo4J
(Users, Recordables, Postables, Plants) ^JeSQgPHk

GraphQL
(Mutate, Read data) ^2eL9qBlg

Plant / Garden Data ^iU4tJ68m

PostgreSQL
(Job / Task Queue ^99XOWjbX

RabbitMQ Job queue (Worker API) ^uQ0Yy6HL

Cloudinary (Images) ^UxQRl6iS

Update
New Jobs / Tasks ^5xY4lNcN

Received Status code (0, plant data) ^ui91WeOI

Execute
Jobs ^sOMbg6ZX

Received Status code (0, plant data) ^hfRbT7Az

InfluxDB
(All data on Ceres, recorded and logged for further analytics) ^DvRhhhsh

Vue.JS (Viewing analytics) ^Y7Ys3Xyf

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.0.7",
	"elements": [
		{
			"id": "rZ5dZDL5lei0rcBYYDKUk",
			"type": "rectangle",
			"x": -253.46426966826346,
			"y": -392.3446609507346,
			"width": 183.25623185765676,
			"height": 160.56131347828904,
			"angle": 0,
			"strokeColor": "#343a40",
			"backgroundColor": "#1e1e1e",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 1989997676,
			"version": 794,
			"versionNonce": 880068692,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "nMwfUyhR"
				},
				{
					"id": "b5z0SoqSDLY2b7kHdblGt",
					"type": "arrow"
				},
				{
					"id": "YpkGJEF2vc1wWjgt5YwN-",
					"type": "arrow"
				},
				{
					"id": "VJXVhk2O-1QOnATu1pYkz",
					"type": "arrow"
				},
				{
					"id": "2VhaaqbOyJwCgYmJGgxr3",
					"type": "arrow"
				},
				{
					"id": "DA-4mCfC2e7rkB5Vdk7Vy",
					"type": "arrow"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false
		},
		{
			"id": "nMwfUyhR",
			"type": "text",
			"x": -200.17611955974758,
			"y": -324.56400421159003,
			"width": 76.679931640625,
			"height": 25,
			"angle": 0,
			"strokeColor": "#ffffff",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1066240876,
			"version": 654,
			"versionNonce": 274306796,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Next.JS",
			"rawText": "Next.JS",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "rZ5dZDL5lei0rcBYYDKUk",
			"originalText": "Next.JS",
			"lineHeight": 1.25
		},
		{
			"id": "b5z0SoqSDLY2b7kHdblGt",
			"type": "arrow",
			"x": -53.87931040831366,
			"y": -307.6115877344762,
			"width": 293.11843239829466,
			"height": 209.82259631318226,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 363055468,
			"version": 1937,
			"versionNonce": 1826659796,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "2eL9qBlg"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					174.9432165594327,
					39.9331135407171
				],
				[
					293.11843239829466,
					209.82259631318226
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "rZ5dZDL5lei0rcBYYDKUk",
				"focus": -0.199515485227597,
				"gap": 16.328727402293026
			},
			"endBinding": {
				"elementId": "8D5qHvVoOHWqMH1uBiq7Z",
				"gap": 18.332391001644126,
				"focus": -0.3479514008441475
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "2eL9qBlg",
			"type": "text",
			"x": -45.38797807907301,
			"y": -372.32971489049487,
			"width": 212.33985900878906,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 241599468,
			"version": 59,
			"versionNonce": 1163909484,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "GraphQL\n(Mutate, Read data)",
			"rawText": "GraphQL\n(Mutate, Read data)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "b5z0SoqSDLY2b7kHdblGt",
			"originalText": "GraphQL\n(Mutate, Read data)",
			"lineHeight": 1.25
		},
		{
			"id": "8D5qHvVoOHWqMH1uBiq7Z",
			"type": "ellipse",
			"x": 167.73091193538826,
			"y": -104.33329891965298,
			"width": 429.3742204636894,
			"height": 199.08363091878368,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#99e9f2",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1447199572,
			"version": 682,
			"versionNonce": 607503188,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "JeSQgPHk"
				},
				{
					"id": "b5z0SoqSDLY2b7kHdblGt",
					"type": "arrow"
				},
				{
					"id": "YpkGJEF2vc1wWjgt5YwN-",
					"type": "arrow"
				},
				{
					"id": "Hm0vgPZJ0dkqv_ry91XeB",
					"type": "arrow"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false
		},
		{
			"id": "JeSQgPHk",
			"type": "text",
			"x": 282.47139522264314,
			"y": -42.17817618321702,
			"width": 200.2798309326172,
			"height": 75,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2030768236,
			"version": 779,
			"versionNonce": 897174508,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Neo4J\n(Users, Recordables,\nPostables, Plants)",
			"rawText": "Neo4J\n(Users, Recordables, Postables, Plants)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 68,
			"containerId": "8D5qHvVoOHWqMH1uBiq7Z",
			"originalText": "Neo4J\n(Users, Recordables, Postables, Plants)",
			"lineHeight": 1.25
		},
		{
			"id": "YpkGJEF2vc1wWjgt5YwN-",
			"type": "arrow",
			"x": 412.12400689847834,
			"y": -127.38018208017488,
			"width": 468.2086659599956,
			"height": 268.7588551452557,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 614592084,
			"version": 1417,
			"versionNonce": 2110832876,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "iU4tJ68m"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-191.5569586607436,
					-255.0558179553466
				],
				[
					-468.2086659599956,
					-268.7588551452557
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "8D5qHvVoOHWqMH1uBiq7Z",
				"gap": 24.00739895783415,
				"focus": 0.5356712630828726
			},
			"endBinding": {
				"elementId": "rZ5dZDL5lei0rcBYYDKUk",
				"focus": -1.0529825084571283,
				"gap": 14.624196340456024
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "iU4tJ68m",
			"type": "text",
			"x": -70.55329606143471,
			"y": -365.9063328247319,
			"width": 215.41989135742188,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 44319340,
			"version": 48,
			"versionNonce": 357110740,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Plant / Garden Data",
			"rawText": "Plant / Garden Data",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "YpkGJEF2vc1wWjgt5YwN-",
			"originalText": "Plant / Garden Data",
			"lineHeight": 1.25
		},
		{
			"id": "VJXVhk2O-1QOnATu1pYkz",
			"type": "arrow",
			"x": -251.487838674272,
			"y": -420.25993265590273,
			"width": 311.0305476713636,
			"height": 336.5313878274253,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 25882836,
			"version": 2308,
			"versionNonce": 241288044,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "5xY4lNcN"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-202.74638441189532,
					-252.32272179177608
				],
				[
					-311.0305476713636,
					-336.5313878274253
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "rZ5dZDL5lei0rcBYYDKUk",
				"focus": -0.017382874200405747,
				"gap": 27.91527170516818
			},
			"endBinding": {
				"elementId": "ahOcU_1pyXQ4yk5LZ2p48",
				"focus": -0.32966943436985663,
				"gap": 26.843013118057513
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "5xY4lNcN",
			"type": "text",
			"x": -466.55988116281304,
			"y": -696.806206682031,
			"width": 181.6598663330078,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 548057324,
			"version": 80,
			"versionNonce": 1411348820,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Update\nNew Jobs / Tasks",
			"rawText": "Update\nNew Jobs / Tasks",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "VJXVhk2O-1QOnATu1pYkz",
			"originalText": "Update\nNew Jobs / Tasks",
			"lineHeight": 1.25
		},
		{
			"id": "d5jnOd6hhOrGrlt8_WZ1B",
			"type": "rectangle",
			"x": -765.8273150754686,
			"y": -324.17399733533387,
			"width": 194.7526478927,
			"height": 388.691202877583,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#4dabf7",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 118146260,
			"version": 814,
			"versionNonce": 447641068,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "99XOWjbX"
				},
				{
					"id": "tjPi3ErgON7HSQf7_uPrY",
					"type": "arrow"
				},
				{
					"id": "KiSsslSieBKRZy9Hgnhav",
					"type": "arrow"
				},
				{
					"id": "PmSw85RyeNE-tnWws1TNq",
					"type": "arrow"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false
		},
		{
			"id": "99XOWjbX",
			"type": "text",
			"x": -729.5709480993335,
			"y": -167.32839589654236,
			"width": 122.23991394042969,
			"height": 75,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 622124884,
			"version": 743,
			"versionNonce": 101527252,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "PostgreSQL\n(Job / Task\nQueue",
			"rawText": "PostgreSQL\n(Job / Task Queue",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 68,
			"containerId": "d5jnOd6hhOrGrlt8_WZ1B",
			"originalText": "PostgreSQL\n(Job / Task Queue",
			"lineHeight": 1.25
		},
		{
			"id": "ahOcU_1pyXQ4yk5LZ2p48",
			"type": "ellipse",
			"x": -771.02878749446,
			"y": -895.3849265410066,
			"width": 192.80645703494997,
			"height": 180.07353514862393,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1134385492,
			"version": 873,
			"versionNonce": 264296556,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "uQ0Yy6HL"
				},
				{
					"id": "VJXVhk2O-1QOnATu1pYkz",
					"type": "arrow"
				},
				{
					"id": "tjPi3ErgON7HSQf7_uPrY",
					"type": "arrow"
				},
				{
					"id": "DA-4mCfC2e7rkB5Vdk7Vy",
					"type": "arrow"
				},
				{
					"id": "KiSsslSieBKRZy9Hgnhav",
					"type": "arrow"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false
		},
		{
			"id": "uQ0Yy6HL",
			"type": "text",
			"x": -724.5128910343038,
			"y": -855.5137678746077,
			"width": 99.43991088867188,
			"height": 100,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 983078612,
			"version": 885,
			"versionNonce": 1806185556,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "RabbitMQ\nJob queue\n(Worker\nAPI)",
			"rawText": "RabbitMQ Job queue (Worker API)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 93,
			"containerId": "ahOcU_1pyXQ4yk5LZ2p48",
			"originalText": "RabbitMQ Job queue (Worker API)",
			"lineHeight": 1.25
		},
		{
			"id": "tjPi3ErgON7HSQf7_uPrY",
			"type": "arrow",
			"x": -624.2390553297066,
			"y": -702.8859325418317,
			"width": 6.862009922393895,
			"height": 366.81195109727673,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1656474964,
			"version": 1864,
			"versionNonce": 694348524,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "sOMbg6ZX"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					6.862009922393895,
					234.06285112574358
				],
				[
					5.228197610356915,
					366.81195109727673
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "ahOcU_1pyXQ4yk5LZ2p48",
				"focus": -0.49132038714936765,
				"gap": 22.984287636810123
			},
			"endBinding": {
				"elementId": "d5jnOd6hhOrGrlt8_WZ1B",
				"focus": 0.47010715258291474,
				"gap": 11.899984109221066
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "sOMbg6ZX",
			"type": "text",
			"x": -692.1431681402757,
			"y": -551.6843710878401,
			"width": 79.2999267578125,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1186945108,
			"version": 41,
			"versionNonce": 1120307668,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Execute\nJobs",
			"rawText": "Execute\nJobs",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "tjPi3ErgON7HSQf7_uPrY",
			"originalText": "Execute\nJobs",
			"lineHeight": 1.25
		},
		{
			"id": "KiSsslSieBKRZy9Hgnhav",
			"type": "arrow",
			"x": -744.6090218920839,
			"y": -341.93377302438245,
			"width": 2.7539623265619184,
			"height": 374.60420595849035,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 138191084,
			"version": 2691,
			"versionNonce": 438218092,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "hfRbT7Az"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.9709787526833225,
					-269.2844726960371
				],
				[
					-2.7539623265619184,
					-374.60420595849035
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "d5jnOd6hhOrGrlt8_WZ1B",
				"focus": -0.7687139450394183,
				"gap": 17.759775689048524
			},
			"endBinding": {
				"elementId": "ahOcU_1pyXQ4yk5LZ2p48",
				"focus": 0.7700119330460202,
				"gap": 22.32784336527712
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "hfRbT7Az",
			"type": "text",
			"x": -807.6287709686019,
			"y": -549.7636138655917,
			"width": 216.39981079101562,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 407071084,
			"version": 8,
			"versionNonce": 511586132,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Received Status code\n(0, plant data)",
			"rawText": "Received Status code (0, plant data)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "KiSsslSieBKRZy9Hgnhav",
			"originalText": "Received Status code (0, plant data)",
			"lineHeight": 1.25
		},
		{
			"id": "DA-4mCfC2e7rkB5Vdk7Vy",
			"type": "arrow",
			"x": -550.5592598643113,
			"y": -828.5407750814037,
			"width": 364.9720890093142,
			"height": 406.94886217518865,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 380059244,
			"version": 1021,
			"versionNonce": 1059398868,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "ui91WeOI"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					282.30441368871993,
					113.51665732896925
				],
				[
					364.9720890093142,
					406.94886217518865
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "ahOcU_1pyXQ4yk5LZ2p48",
				"focus": -0.7455144463214466,
				"gap": 30.04396475467358
			},
			"endBinding": {
				"elementId": "rZ5dZDL5lei0rcBYYDKUk",
				"focus": 0.062198740941483276,
				"gap": 29.247251955480493
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "ui91WeOI",
			"type": "text",
			"x": -467.72916200628794,
			"y": -767.8322523597483,
			"width": 253.59979248046875,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1563906644,
			"version": 67,
			"versionNonce": 214647404,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Received Status code (0,\nplant data)",
			"rawText": "Received Status code (0, plant data)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "DA-4mCfC2e7rkB5Vdk7Vy",
			"originalText": "Received Status code (0, plant data)",
			"lineHeight": 1.25
		},
		{
			"id": "2VhaaqbOyJwCgYmJGgxr3",
			"type": "arrow",
			"x": -108.48433944613551,
			"y": -409.2806488351415,
			"width": 195.0874458443249,
			"height": 233.47589769172157,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 905848940,
			"version": 1089,
			"versionNonce": 1783720532,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					195.0874458443249,
					-233.47589769172157
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "rZ5dZDL5lei0rcBYYDKUk",
				"gap": 16.935987884406956,
				"focus": -0.17647361863101207
			},
			"endBinding": {
				"elementId": "jZKORbGtPBw7MkX_LNO5-",
				"gap": 9.577440491423957,
				"focus": 0.6702189367328524
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "jZKORbGtPBw7MkX_LNO5-",
			"type": "ellipse",
			"x": 75.33136235882148,
			"y": -713.8560773668244,
			"width": 234.6525061202965,
			"height": 85,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1162664276,
			"version": 327,
			"versionNonce": 764417260,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "UxQRl6iS"
				},
				{
					"id": "2VhaaqbOyJwCgYmJGgxr3",
					"type": "arrow"
				}
			],
			"updated": 1702261068615,
			"link": null,
			"locked": false
		},
		{
			"id": "UxQRl6iS",
			"type": "text",
			"x": 144.19547204529715,
			"y": -696.4081155672526,
			"width": 96.99990844726562,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1217342164,
			"version": 323,
			"versionNonce": 1121517524,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"text": "Cloudinary\n(Images)",
			"rawText": "Cloudinary (Images)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "jZKORbGtPBw7MkX_LNO5-",
			"originalText": "Cloudinary (Images)",
			"lineHeight": 1.25
		},
		{
			"id": "PmSw85RyeNE-tnWws1TNq",
			"type": "arrow",
			"x": -551.0576648358949,
			"y": 65.98798012071192,
			"width": 146.19391167479284,
			"height": 164.46742792314188,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 386468972,
			"version": 694,
			"versionNonce": 857118572,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					146.19391167479284,
					164.46742792314188
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "d5jnOd6hhOrGrlt8_WZ1B",
				"focus": 0.20977568629939525,
				"gap": 20.01700234687371
			},
			"endBinding": {
				"elementId": "jgi_kQ_G-GSCMMXl5cEp8",
				"focus": -0.2088801902792359,
				"gap": 26.156390473643114
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "Hm0vgPZJ0dkqv_ry91XeB",
			"type": "arrow",
			"x": 233.17559480663263,
			"y": 88.56797196216849,
			"width": 146.93521739502984,
			"height": 151.50721781795576,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 765353044,
			"version": 1424,
			"versionNonce": 550626644,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-146.93521739502984,
					151.50721781795576
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "8D5qHvVoOHWqMH1uBiq7Z",
				"gap": 21.06135924759006,
				"focus": 0.24865066817301407
			},
			"endBinding": {
				"elementId": "jgi_kQ_G-GSCMMXl5cEp8",
				"gap": 16.536608737372717,
				"focus": 0.15646652812694292
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "jgi_kQ_G-GSCMMXl5cEp8",
			"type": "rectangle",
			"x": -601.5534375508689,
			"y": 256.61179851749694,
			"width": 909.414458946327,
			"height": 260.175977468007,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#f8f0fc",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 1319788524,
			"version": 314,
			"versionNonce": 2041104876,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "PmSw85RyeNE-tnWws1TNq",
					"type": "arrow"
				},
				{
					"id": "Hm0vgPZJ0dkqv_ry91XeB",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "DvRhhhsh"
				},
				{
					"id": "xckQez3xCCTyoCxE5XreX",
					"type": "arrow"
				}
			],
			"updated": 1702261068615,
			"link": null,
			"locked": false
		},
		{
			"id": "DvRhhhsh",
			"type": "text",
			"x": -457.1459822476273,
			"y": 361.6997872515004,
			"width": 620.5995483398438,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#f8f0fc",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 2110737900,
			"version": 330,
			"versionNonce": 1771182804,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"text": "InfluxDB\n(All data on Ceres, recorded and logged for further analytics)",
			"rawText": "InfluxDB\n(All data on Ceres, recorded and logged for further analytics)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "jgi_kQ_G-GSCMMXl5cEp8",
			"originalText": "InfluxDB\n(All data on Ceres, recorded and logged for further analytics)",
			"lineHeight": 1.25
		},
		{
			"id": "xckQez3xCCTyoCxE5XreX",
			"type": "arrow",
			"x": -144.64067473856107,
			"y": 236.9523201877912,
			"width": 3.280274382683956,
			"height": 136.7165254617911,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#1e1e1e",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1269659372,
			"version": 620,
			"versionNonce": 1146137708,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-3.280274382683956,
					-136.7165254617911
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "jgi_kQ_G-GSCMMXl5cEp8",
				"focus": 0.012655050354933188,
				"gap": 19.659478329705735
			},
			"endBinding": {
				"elementId": "CNGBuw4YHowGP41e6hW5S",
				"focus": 0.03569064969987015,
				"gap": 16.251272718814675
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "CNGBuw4YHowGP41e6hW5S",
			"type": "rectangle",
			"x": -253.0140841940123,
			"y": -102.40741988966931,
			"width": 212.6847498299676,
			"height": 186.39194189685475,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#69db7c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 1499332436,
			"version": 147,
			"versionNonce": 1739563092,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "xckQez3xCCTyoCxE5XreX",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "Y7Ys3Xyf"
				}
			],
			"updated": 1702261068615,
			"link": null,
			"locked": false
		},
		{
			"id": "Y7Ys3Xyf",
			"type": "text",
			"x": -218.09164641281757,
			"y": -34.21144894124194,
			"width": 142.83987426757812,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#69db7c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 447364180,
			"version": 116,
			"versionNonce": 1691893484,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"text": "Vue.JS (Viewing\nanalytics)",
			"rawText": "Vue.JS (Viewing analytics)",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 43,
			"containerId": "CNGBuw4YHowGP41e6hW5S",
			"originalText": "Vue.JS (Viewing analytics)",
			"lineHeight": 1.25
		},
		{
			"id": "v_rXBMQ1ePYbtOrqe0bUy",
			"type": "ellipse",
			"x": 319.9310086892352,
			"y": 104.68386085016954,
			"width": 342.82439009993044,
			"height": 331.96246800788634,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#99e9f2",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 306783188,
			"version": 715,
			"versionNonce": 1355430100,
			"isDeleted": true,
			"boundElements": [
				{
					"id": "b5z0SoqSDLY2b7kHdblGt",
					"type": "arrow"
				},
				{
					"id": "YpkGJEF2vc1wWjgt5YwN-",
					"type": "arrow"
				},
				{
					"id": "Hm0vgPZJ0dkqv_ry91XeB",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "RtiQTwoX"
				}
			],
			"updated": 1702261068614,
			"link": null,
			"locked": false
		},
		{
			"id": "RtiQTwoX",
			"type": "text",
			"x": 378.2299099458111,
			"y": 243.69583500189555,
			"width": 225.81313659097552,
			"height": 54.20560747663553,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1831514988,
			"version": 818,
			"versionNonce": 582053484,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "Neo4J\n(Users, Recordables, Postables,\nPlants)",
			"rawText": "Neo4J\n(Users, Recordables, Postables, Plants)",
			"fontSize": 14.45482866043614,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 49,
			"containerId": "v_rXBMQ1ePYbtOrqe0bUy",
			"originalText": "Neo4J\n(Users, Recordables, Postables, Plants)",
			"lineHeight": 1.25
		},
		{
			"id": "XAYDdxoYuzQHSyY3PPwlQ",
			"type": "arrow",
			"x": 209.59427896179523,
			"y": -191.17035348492396,
			"width": 19.460343526356496,
			"height": 60.25745069469997,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1416779372,
			"version": 32,
			"versionNonce": 775159380,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					19.460343526356496,
					-60.25745069469997
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "8D5qHvVoOHWqMH1uBiq7Z",
				"focus": 0.21058763820063076,
				"gap": 14.553303041721897
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "SYUP1ju9",
			"type": "text",
			"x": -704.4288655730941,
			"y": -537.2636138655917,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 45888980,
			"version": 15,
			"versionNonce": 859709420,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1702261068614,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "KiSsslSieBKRZy9Hgnhav",
			"originalText": "",
			"lineHeight": 1.25
		},
		{
			"id": "1PdztVm7YzyJgiU-J0BBG",
			"type": "freedraw",
			"x": -283.4346088631153,
			"y": -407.53120822988154,
			"width": 519.0395421112146,
			"height": 548.6803117756797,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#69db7c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1723291604,
			"version": 228,
			"versionNonce": 587232724,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1702261068615,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.25007100957611783,
					0
				],
				[
					6.815660849231904,
					0
				],
				[
					18.622935183728487,
					0
				],
				[
					36.66727469824161,
					0
				],
				[
					54.270312431149705,
					0
				],
				[
					70.4023442253748,
					0
				],
				[
					86.36766201321575,
					0
				],
				[
					99.861689824069,
					0
				],
				[
					119.68104317125972,
					0
				],
				[
					127.45776123376459,
					0.009806706257847964
				],
				[
					143.11416777448198,
					2.7556844584663622
				],
				[
					150.79772212753687,
					4.5503117036598155
				],
				[
					164.28194323213233,
					7.97775554079152
				],
				[
					170.58275200282503,
					9.639992251503429
				],
				[
					176.81491382971262,
					11.282615549699642
				],
				[
					183.43444055378666,
					12.55258401009604
				],
				[
					195.29074841957276,
					14.651219149283975
				],
				[
					200.84624751466606,
					15.622083068814845
				],
				[
					210.48623976616955,
					18.382670880410217
				],
				[
					215.3699794825975,
					19.86838687848018
				],
				[
					219.27795192636574,
					21.23151904832656
				],
				[
					223.07805060129715,
					22.58974786504399
				],
				[
					227.2655141734151,
					24.296114753916413
				],
				[
					231.12935643902279,
					26.247649299236002
				],
				[
					234.9294551139542,
					28.61106550738691
				],
				[
					238.3568989510859,
					31.106872250019308
				],
				[
					241.54898183802834,
					33.602678992651704
				],
				[
					244.5988674842314,
					36.564304282533726
				],
				[
					247.12409434563745,
					39.78580728824977
				],
				[
					252.60604314379657,
					47.69981923836502
				],
				[
					255.13617335833158,
					52.55904218914833
				],
				[
					257.57804321654555,
					58.34499888130199
				],
				[
					259.9218460121807,
					64.101535454682
				],
				[
					262.2313253359132,
					69.90710555935141
				],
				[
					264.3495738876169,
					76.1932042706573
				],
				[
					266.57079285502846,
					83.89146868309905
				],
				[
					269.228410250916,
					92.40368971494547
				],
				[
					273.4845207668392,
					109.44284183802506
				],
				[
					275.3085681308063,
					117.24898001930359
				],
				[
					277.0443551384524,
					124.19703140301692
				],
				[
					278.73110861480905,
					130.7233944176411
				],
				[
					280.388441972392,
					137.0192998352049
				],
				[
					282.1095189206512,
					143.79573385940517
				],
				[
					283.43832761859505,
					150.77320536189217
				],
				[
					284.37486806622326,
					157.60847962363982
				],
				[
					285.29179510133577,
					165.1400300296974
				],
				[
					286.5568602086032,
					173.48553705515974
				],
				[
					287.9396057909654,
					183.78257862594165
				],
				[
					288.49858804766495,
					194.94751370054666
				],
				[
					289.018343479333,
					205.1808116806523
				],
				[
					289.5135821453564,
					215.48765995769213
				],
				[
					290.13140463960326,
					228.40799545245898
				],
				[
					290.74922713385024,
					242.4757155793987
				],
				[
					290.74922713385024,
					255.86677297454418
				],
				[
					290.74922713385024,
					273.6267180075785
				],
				[
					290.74922713385024,
					290.3765722960505
				],
				[
					290.74922713385024,
					303.5028486222329
				],
				[
					290.74922713385024,
					320.59593762973094
				],
				[
					290.74922713385024,
					336.0365966327749
				],
				[
					290.74922713385024,
					348.5646638772262
				],
				[
					290.4795427117583,
					361.7350703815692
				],
				[
					289.4792586734537,
					373.4786011253896
				],
				[
					288.00334938164167,
					383.84919299310565
				],
				[
					286.29207913964035,
					393.09201364116467
				],
				[
					283.8796294002,
					402.2612839922895
				],
				[
					281.4573729545017,
					411.34229398709346
				],
				[
					279.2067338683165,
					419.310242821627
				],
				[
					276.80409083513405,
					425.8267991299933
				],
				[
					274.4995148645305,
					431.6814027659522
				],
				[
					272.17532548141116,
					437.2859353923349
				],
				[
					269.8756528639366,
					442.8561445468151
				],
				[
					267.5759802464619,
					448.4263537012952
				],
				[
					265.37927804469507,
					453.31499677085213
				],
				[
					263.5601340338569,
					457.6103341118068
				],
				[
					261.80473361369513,
					461.78308762453804
				],
				[
					259.73551859328086,
					465.52434606192213
				],
				[
					257.5633331571587,
					469.44212521194817
				],
				[
					255.0528163551395,
					473.732559199774
				],
				[
					252.5619129656361,
					477.9788630094393
				],
				[
					250.33579064509559,
					481.72502479995234
				],
				[
					248.14889514958668,
					484.85826744934747
				],
				[
					244.63319095613394,
					489.6733602219702
				],
				[
					242.56887928884862,
					491.8602557174791
				],
				[
					240.59282797788433,
					493.67939972831726
				],
				[
					238.60696996066213,
					495.361249851545
				],
				[
					235.8365754428088,
					497.02838991538584
				],
				[
					232.74746297157424,
					498.79359704180564
				],
				[
					229.47202308143983,
					500.52448069632277
				],
				[
					225.64740764086366,
					502.3485280602899
				],
				[
					221.48446083439046,
					504.1088318335807
				],
				[
					217.1744134340488,
					505.8936523725162
				],
				[
					212.92320627125457,
					507.658859498936
				],
				[
					208.6670957553314,
					509.4240666253558
				],
				[
					200.03719424839028,
					512.6455696310718
				],
				[
					195.1779712976071,
					514.1214789228839
				],
				[
					189.00955306139576,
					515.7150686897905
				],
				[
					181.91440108381414,
					517.4557590505656
				],
				[
					174.00038913369883,
					519.3043231801776
				],
				[
					165.4734580424656,
					521.0793370128551
				],
				[
					157.510412561061,
					522.3345954138647
				],
				[
					150.42506728973717,
					523.2515224489773
				],
				[
					138.4461755957276,
					524.7960786845945
				],
				[
					131.8707790498139,
					525.1540234630074
				],
				[
					116.97929559721172,
					525.1540234630074
				],
				[
					105.86829740702512,
					525.1540234630074
				],
				[
					92.61453389949008,
					525.1540234630074
				],
				[
					78.58604059758193,
					525.1540234630074
				],
				[
					63.478809607306175,
					524.3302601373448
				],
				[
					48.87172063618266,
					522.1580747012229
				],
				[
					33.81842653032527,
					519.0983823487618
				],
				[
					18.07866298641568,
					515.5728714490513
				],
				[
					1.3729388761042856,
					511.7531593616041
				],
				[
					-14.450181670997267,
					507.54608237697033
				],
				[
					-29.32695506421271,
					502.88789690447373
				],
				[
					-43.468225488086546,
					498.19538796007447
				],
				[
					-53.79959053077107,
					494.12560486305114
				],
				[
					-66.92586685695358,
					487.7365357360374
				],
				[
					-72.10380776111822,
					484.45619249277405
				],
				[
					-78.18886899413747,
					479.99904449856416
				],
				[
					-84.4455475866697,
					475.42421602925964
				],
				[
					-90.28053781011278,
					469.9422672311005
				],
				[
					-95.68893631133778,
					464.0337267107232
				],
				[
					-101.11204487194959,
					458.3115136092459
				],
				[
					-107.52072741147913,
					451.2114582785353
				],
				[
					-114.13535078242427,
					442.4393595308548
				],
				[
					-119.97524435899629,
					433.02001817015383
				],
				[
					-125.19731544132145,
					423.8262310533842
				],
				[
					-130.03202162646,
					414.9266451243513
				],
				[
					-134.1900650798043,
					406.8312091084651
				],
				[
					-138.142167701733,
					399.50559953382316
				],
				[
					-142.12369044243536,
					392.93020298790947
				],
				[
					-146.15424671442713,
					386.3057729107065
				],
				[
					-151.01346966521038,
					378.2495637198519
				],
				[
					-156.56406540717478,
					369.68340580358705
				],
				[
					-162.07543432410756,
					360.9897607059698
				],
				[
					-166.7385231497331,
					353.0414252839519
				],
				[
					-170.55333188405137,
					346.74551986638807
				],
				[
					-174.7456988092983,
					339.8857288389957
				],
				[
					-178.90864561577155,
					332.5895393831274
				],
				[
					-186.86678774404731,
					319.39461611313965
				],
				[
					-190.98560437236006,
					311.66202822879535
				],
				[
					-195.13384111944652,
					303.6842726880039
				],
				[
					-198.78683920050963,
					295.7261305597281
				],
				[
					-202.72913511618043,
					286.29698249276925
				],
				[
					-206.54884720362764,
					276.33336893475547
				],
				[
					-210.07435810333817,
					266.2716883141629
				],
				[
					-213.5949656499198,
					256.3276881686649
				],
				[
					-218.81703673224496,
					238.8668476764961
				],
				[
					-220.82250816198297,
					231.1538732046675
				],
				[
					-222.13660680053988,
					224.8432577277169
				],
				[
					-223.38696184842053,
					219.14065803875525
				],
				[
					-224.6814470744617,
					212.2808670113629
				],
				[
					-225.73076464405568,
					204.5629891864054
				],
				[
					-226.78988891990753,
					196.5558135268402
				],
				[
					-227.79017295821205,
					188.00436566996228
				],
				[
					-228.29031497736435,
					180.28158449187583
				],
				[
					-228.29031497736435,
					172.82848773588125
				],
				[
					-228.29031497736435,
					164.41433376661377
				],
				[
					-228.29031497736435,
					155.82365908470427
				],
				[
					-228.29031497736435,
					146.46315796155062
				],
				[
					-228.29031497736435,
					136.23966668770282
				],
				[
					-228.29031497736435,
					127.14394663351209
				],
				[
					-228.21676468043017,
					119.62220293371234
				],
				[
					-227.78036625195415,
					113.37042769430906
				],
				[
					-226.81440568555223,
					108.49649468413895
				],
				[
					-224.5490565399802,
					98.39558723851474
				],
				[
					-223.4507054390968,
					93.21764633435015
				],
				[
					-222.12189674115308,
					87.4905298797438
				],
				[
					-220.53811368050424,
					81.83206036894268
				],
				[
					-218.88568367605018,
					75.55086501076568
				],
				[
					-216.95376254324634,
					69.21082941504142
				],
				[
					-210.27539558162488,
					55.79525525425123
				],
				[
					-206.00457500631484,
					47.74394941652554
				],
				[
					-201.04238163982376,
					39.388635684805365
				],
				[
					-195.09461429441495,
					31.312813081434967
				],
				[
					-188.97522958949315,
					23.143826768614588
				],
				[
					-178.2074661183326,
					10.993317715091962
				],
				[
					-173.30411298938884,
					6.251775239403287
				],
				[
					-160.43771437904036,
					-2.6429073365006843
				],
				[
					-154.3183296741185,
					-6.967664796229144
				],
				[
					-148.62063333828587,
					-10.650082996065919
				],
				[
					-143.8790908625972,
					-13.25376350753504
				],
				[
					-140.01524859698952,
					-15.136651109049467
				],
				[
					-136.62212823176043,
					-16.308552506866988
				],
				[
					-132.86615973498948,
					-17.044055476208598
				],
				[
					-128.04616360923774,
					-18.054146220770974
				],
				[
					-122.23078679831042,
					-19.30940462178063
				],
				[
					-116.2437926278701,
					-20.36362554450352
				],
				[
					-110.93346118922398,
					-21.04028827629776
				],
				[
					-105.961461116475,
					-21.613980592384166
				],
				[
					-100.97475098433915,
					-22.182769555341622
				],
				[
					-95.98804085220337,
					-22.535810980625627
				],
				[
					-91.0650743107438,
					-22.673104868236067
				],
				[
					-85.91655352535281,
					-23.09969659045413
				],
				[
					-80.97887692450644,
					-23.52628831267225
				],
				[
					-77.13464807141452,
					-23.52628831267225
				],
				[
					-73.37377622151462,
					-23.52628831267225
				],
				[
					-69.5246440152938,
					-23.52628831267225
				],
				[
					-65.84222581545703,
					-23.52628831267225
				],
				[
					-61.04184310222104,
					-23.52628831267225
				],
				[
					-55.358856825775206,
					-23.52628831267225
				],
				[
					-50.40647016554203,
					-23.52628831267225
				],
				[
					-46.09642276520037,
					-23.52628831267225
				],
				[
					-41.21758640190137,
					-23.104599943583082
				],
				[
					-36.79966523272299,
					-22.33477350233892
				],
				[
					-33.028986676565296,
					-21.56985041422371
				],
				[
					-29.876130614654357,
					-20.667633438498058
				],
				[
					-27.635298234727088,
					-19.907613703511743
				],
				[
					-25.384659148541914,
					-19.35843815307004
				],
				[
					-23.48215813451168,
					-18.975976609012434
				],
				[
					-21.927795192636495,
					-18.578805005567972
				],
				[
					-20.18710483186146,
					-18.039436161384174
				],
				[
					-18.260087052186577,
					-17.529487435973977
				],
				[
					-16.176161972385444,
					-16.872438116695548
				],
				[
					-13.920619533071317,
					-16.02415802538826
				],
				[
					-11.885727984559708,
					-15.058197458986342
				],
				[
					-9.973420264271567,
					-14.200110661421206
				],
				[
					-6.899017852423867,
					-12.768331547769606
				],
				[
					-5.6290493920274685,
					-12.047538637814853
				],
				[
					-4.452244641080938,
					-11.365972552891662
				],
				[
					-3.6431913748051556,
					-10.963897596318304
				],
				[
					-3.2165996525870924,
					-10.76776347116055
				],
				[
					-3.3587968933264847,
					-10.743246705515844
				],
				[
					-4.555215056788711,
					-10.743246705515844
				],
				[
					-4.555215056788711,
					-10.743246705515844
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-4.555215056788711,
				-10.743246705515844
			]
		},
		{
			"id": "jX5lYii2BAHCtFJ-2Wiar",
			"type": "freedraw",
			"x": -335.2671150591019,
			"y": -363.02762719125917,
			"width": 562.2467027644309,
			"height": 499.19982731117926,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#69db7c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 949656404,
			"version": 282,
			"versionNonce": 1230708972,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1702261110088,
			"link": null,
			"locked": false,
			"points": [
				[
					-17.3554855962075,
					-1.4558113479783046
				],
				[
					-17.243094180461195,
					-1.4558113479783046
				],
				[
					-16.127825516517042,
					-1.4558113479783046
				],
				[
					-10.331022112063142,
					-1.4558113479783046
				],
				[
					5.256802702598023,
					-1.4558113479783046
				],
				[
					31.997314156700718,
					-1.5917220006630564
				],
				[
					64.002931163842,
					-3.576017529860799
				],
				[
					97.24917649095214,
					-7.830020958894237
				],
				[
					160.45205686121326,
					-16.54189379598832
				],
				[
					180.62631598767578,
					-18.46729470902266
				],
				[
					195.80347986018694,
					-19.137787262267523
				],
				[
					208.78901112564515,
					-19.137787262267523
				],
				[
					229.70245994797762,
					-19.137787262267523
				],
				[
					239.69665045588025,
					-19.137787262267523
				],
				[
					258.1677473591102,
					-19.137787262267523
				],
				[
					266.63168551415913,
					-18.84784453654002
				],
				[
					274.40398418769234,
					-18.45823399884366
				],
				[
					281.2036648403441,
					-17.968955649178447
				],
				[
					288.05089570735004,
					-17.11724889235388
				],
				[
					295.43414717253063,
					-15.862340532564456
				],
				[
					302.1430501432334,
					-14.258594830884123
				],
				[
					315.21503634388114,
					-11.005799876628505
				],
				[
					320.6919564881339,
					-9.071338253415162
				],
				[
					325.4988508846683,
					-6.937541006264219
				],
				[
					329.8086294038634,
					-4.495679613027768
				],
				[
					333.8763341045278,
					-1.759345138974259
				],
				[
					336.91522507643776,
					1.0766571470480506
				],
				[
					339.8719838599176,
					4.00779688994966
				],
				[
					342.8114516563595,
					6.947997343030323
				],
				[
					345.5996233162199,
					10.427310051760545
				],
				[
					347.99874776772765,
					13.94286560120677
				],
				[
					350.4800044076653,
					17.8525620434388
				],
				[
					357.0635477223434,
					28.462653663030224
				],
				[
					361.12692967624844,
					35.60249328407043
				],
				[
					370.10527469567467,
					52.573203449309325
				],
				[
					374.23782059773134,
					61.28960664149291
				],
				[
					377.8646051289295,
					69.89275095643919
				],
				[
					386.94237332382374,
					92.05977840932607
				],
				[
					391.5979715837766,
					105.2249902993913
				],
				[
					396.1887286423374,
					122.11415407301934
				],
				[
					400.23914235596396,
					137.76653090721595
				],
				[
					403.5157843996449,
					151.47991576310974
				],
				[
					407.05179124889423,
					168.5593544504965
				],
				[
					409.8226719217166,
					184.32499016193043
				],
				[
					413.26357834225746,
					209.2963074152131
				],
				[
					414.2707783372148,
					222.22594084062473
				],
				[
					414.81976717566795,
					234.29480679903278
				],
				[
					415.32120579976686,
					247.82244709625738
				],
				[
					415.6756710340436,
					260.806444782743
				],
				[
					415.6756710340436,
					271.1265936766069
				],
				[
					415.6756710340436,
					281.1024355836694
				],
				[
					415.6756710340436,
					290.53010452490327
				],
				[
					415.6756710340436,
					299.183082745834
				],
				[
					415.44224270903214,
					306.67175970876514
				],
				[
					414.4523337011127,
					313.38574595139295
				],
				[
					412.95234057557536,
					319.6557573952506
				],
				[
					411.4955749176329,
					325.43649048944303
				],
				[
					408.37455175729303,
					335.9016107461707
				],
				[
					406.3342152868215,
					341.12964051944505
				],
				[
					404.2031011344012,
					346.38485242325635
				],
				[
					401.9595955662344,
					352.1882372928963
				],
				[
					399.599375835562,
					359.2737126528626
				],
				[
					396.2924745645648,
					367.1202876678636
				],
				[
					392.5273621370635,
					375.062000139744
				],
				[
					388.8659956317895,
					382.8406198284026
				],
				[
					384.8544866389981,
					391.6023265714811
				],
				[
					380.68303601610626,
					400.7672349175244
				],
				[
					376.4467441918223,
					408.8675098175371
				],
				[
					372.8329279009025,
					415.9756369529508
				],
				[
					369.7810686887142,
					422.36343762913526
				],
				[
					366.7162412362476,
					428.7829507909461
				],
				[
					363.8545828814762,
					435.01671939408783
				],
				[
					361.1528661568053,
					441.45888433134616
				],
				[
					358.4986996464886,
					447.09011570758537
				],
				[
					355.9223425778424,
					451.33958878152924
				],
				[
					353.44540868466413,
					455.0726013752711
				],
				[
					351.2019031164974,
					458.0807571546941
				],
				[
					349.079434457596,
					460.61775600480996
				],
				[
					347.0261297468461,
					462.697188990887
				],
				[
					345.02469799720996,
					464.33264717819384
				],
				[
					342.6039598119047,
					465.69175370504155
				],
				[
					339.79417441824705,
					467.1142852031422
				],
				[
					336.94116155699464,
					468.5368167012428
				],
				[
					329.9037298325718,
					471.0828762615377
				],
				[
					326.1386174050705,
					472.39667923749045
				],
				[
					321.75102944420485,
					473.7874982499647
				],
				[
					317.01329899582197,
					474.8611924061743
				],
				[
					311.5493470918476,
					476.0119025989055
				],
				[
					305.7914484082289,
					477.1897949221736
				],
				[
					300.72951195288556,
					477.90559102631335
				],
				[
					295.7497076859722,
					478.3450354699942
				],
				[
					290.2295600741247,
					478.73464600769057
				],
				[
					282.80740388810875,
					479.01099766814957
				],
				[
					261.48329412362625,
					479.5365188585306
				],
				[
					250.31763924390643,
					480.0620400489118
				],
				[
					229.3177354863845,
					480.0620400489118
				],
				[
					220.46475012298288,
					480.0620400489118
				],
				[
					210.98496647945768,
					480.0620400489118
				],
				[
					200.44610987986147,
					480.0620400489118
				],
				[
					189.52252881867201,
					480.0620400489118
				],
				[
					168.0298319305702,
					480.0620400489118
				],
				[
					158.93045000187863,
					480.0620400489118
				],
				[
					150.53999854174066,
					480.0620400489118
				],
				[
					142.06741489317278,
					480.0620400489118
				],
				[
					134.08762437518482,
					480.0620400489118
				],
				[
					124.70726390712738,
					480.0620400489118
				],
				[
					113.1309480852575,
					480.0620400489118
				],
				[
					102.25923998518185,
					480.0620400489118
				],
				[
					93.5705189986403,
					480.0620400489118
				],
				[
					84.72185638199814,
					480.0620400489118
				],
				[
					75.60086071950919,
					479.953311526764
				],
				[
					67.27525046076333,
					479.5591706339782
				],
				[
					58.789698571916986,
					478.8116620442118
				],
				[
					49.88916299416127,
					478.177412331683
				],
				[
					31.647171669183358,
					476.62803089107655
				],
				[
					25.050660114226872,
					475.96206869292115
				],
				[
					19.236565722735143,
					474.9835119935907
				],
				[
					13.681836136811782,
					473.62440546674287
				],
				[
					8.46428079812734,
					471.83038485130385
				],
				[
					2.957101426558168,
					469.32509848681457
				],
				[
					-3.6739921024740685,
					465.18435393501846
				],
				[
					-11.251767171830998,
					460.1511294305923
				],
				[
					-18.721473572201102,
					454.71470332320126
				],
				[
					-25.18397997761388,
					448.9158488086508
				],
				[
					-31.136402265408798,
					443.13058535936887
				],
				[
					-42.047015086319746,
					428.5247205508451
				],
				[
					-46.90145969720838,
					420.7596919274549
				],
				[
					-54.600271675830584,
					407.83005850204324
				],
				[
					-57.97201414821988,
					402.4253448802787
				],
				[
					-62.42444331047748,
					396.513231488491
				],
				[
					-67.27888792136616,
					390.8457572715359
				],
				[
					-72.07713682438171,
					385.354966903071
				],
				[
					-76.2918149148683,
					380.2719084926604
				],
				[
					-85.0713135833588,
					370.28247552032934
				],
				[
					-90.21538222713222,
					363.75423383637064
				],
				[
					-99.18075900628011,
					351.458850123488
				],
				[
					-102.61734268006147,
					345.69170809456404
				],
				[
					-109.43431431975114,
					334.3884721462802
				],
				[
					-113.00058039631668,
					327.9508375641114
				],
				[
					-116.39825934926276,
					322.32413654296164
				],
				[
					-118.95732543087112,
					318.1833919911656
				],
				[
					-123.59563270378607,
					310.6403507671605
				],
				[
					-125.32473140757537,
					307.20181125423574
				],
				[
					-126.8895657345048,
					303.3374183628986
				],
				[
					-129.08119834155787,
					298.14563143034013
				],
				[
					-131.28147644212982,
					292.43738401757963
				],
				[
					-133.1056755746276,
					286.71101518446113
				],
				[
					-135.37079487659167,
					280.3005627328292
				],
				[
					-137.6618506591126,
					273.11088920580465
				],
				[
					-139.4168858434588,
					265.19182850937165
				],
				[
					-140.69641888426287,
					258.65905647032355
				],
				[
					-142.04079312645916,
					252.85567160068356
				],
				[
					-143.01341114734072,
					247.02510460050675
				],
				[
					-144.44424032472642,
					233.60166247034047
				],
				[
					-144.98890641642004,
					225.0619431266471
				],
				[
					-145.64596392386005,
					215.5980313446972
				],
				[
					-146.5710317303873,
					194.93508178152186
				],
				[
					-146.5710317303873,
					186.04199474084808
				],
				[
					-146.5710317303873,
					177.90094664503005
				],
				[
					-146.5710317303873,
					170.01359843422358
				],
				[
					-146.5710317303873,
					157.1111471393489
				],
				[
					-146.5710317303873,
					151.17638197211375
				],
				[
					-146.5710317303873,
					140.66595816449112
				],
				[
					-146.5710317303873,
					136.04952632829827
				],
				[
					-146.1863072687942,
					127.37389633192015
				],
				[
					-145.50763602755697,
					122.18663975445119
				],
				[
					-144.84193302659799,
					117.00391353207178
				],
				[
					-143.94280170062743,
					106.42553439810675
				],
				[
					-143.2684532061496,
					100.79883337695708
				],
				[
					-141.86356050932076,
					90.00299719869648
				],
				[
					-141.0595296120587,
					85.42733855830903
				],
				[
					-140.02207038978503,
					81.65808312385124
				],
				[
					-139.16184378464982,
					78.82208083782893
				],
				[
					-138.37942662118525,
					76.44364441584537
				],
				[
					-137.63159143179632,
					73.76620455795532
				],
				[
					-135.84197427337432,
					68.79640502478202
				],
				[
					-134.78290131730324,
					66.24581510939771
				],
				[
					-133.71950561447284,
					63.68616448383447
				],
				[
					-132.40971334635236,
					61.12198350318171
				],
				[
					-129.3924361082399,
					56.13859290473995
				],
				[
					-128.15180778827107,
					53.98214388214151
				],
				[
					-127.05383011136477,
					51.46326645238371
				],
				[
					-125.9342387006612,
					48.46870173822914
				],
				[
					-124.48179578947807,
					45.38352992228473
				],
				[
					-123.03799837181401,
					42.316479526698316
				],
				[
					-121.84924301295884,
					39.235838065843396
				],
				[
					-120.4011228485352,
					36.16425731516748
				],
				[
					-119.0091983919847,
					33.11985869502852
				],
				[
					-117.87663874100268,
					30.066399364710556
				],
				[
					-115.46022330245708,
					22.790649090985603
				],
				[
					-114.26714519684232,
					19.27962389662893
				],
				[
					-112.11009456386519,
					13.88397098504337
				],
				[
					-111.16773577029993,
					12.094480724693833
				],
				[
					-109.97898041144475,
					9.417040866803724
				],
				[
					-109.62451517716791,
					8.515500203994705
				],
				[
					-109.32192290400484,
					7.926554042360692
				],
				[
					-109.17494951418267,
					7.459927468142984
				],
				[
					-109.0106851373227,
					6.77584384962962
				],
				[
					-108.83345252018428,
					6.5357350298865144
				],
				[
					-108.8031932928679,
					6.232201238890508
				],
				[
					-108.66054264980534,
					5.915076382626053
				],
				[
					-108.51356925998319,
					5.593421171272044
				],
				[
					-108.36227312340165,
					5.2717659599180875
				],
				[
					-108.13749029190903,
					4.841382226416277
				],
				[
					-107.38965510252011,
					3.4732149893895494
				],
				[
					-106.81472978351019,
					2.607917167296481
				],
				[
					-106.05392635384288,
					1.8060443164563145
				],
				[
					-105.32338215149184,
					1.2216285099118034
				],
				[
					-104.58851520238136,
					0.6372127033672395
				],
				[
					-101.77008431520468,
					-1.1613382671612462
				],
				[
					-99.28450492850745,
					-2.302987749713371
				],
				[
					-92.61450667863998,
					-4.8173348243817244
				],
				[
					-88.84074875761965,
					-6.3259430691827045
				],
				[
					-85.06266808983989,
					-7.9749923217579894
				],
				[
					-80.66643463545539,
					-9.746361161749578
				],
				[
					-75.74714882317464,
					-11.531321067009614
				],
				[
					-70.36532910763022,
					-13.144127478868949
				],
				[
					-64.46910252770846,
					-14.399035838658374
				],
				[
					-58.918695688544574,
					-15.255272950572442
				],
				[
					-53.35964335586173,
					-16.04355473614416
				],
				[
					-48.539780719048835,
					-16.43769562893002
				],
				[
					-44.139224517904864,
					-16.43769562893002
				],
				[
					-39.699763595925646,
					-16.43769562893002
				],
				[
					-36.3409893638148,
					-16.43769562893002
				],
				[
					-32.96492414466607,
					-16.43769562893002
				],
				[
					-29.359753347265197,
					-16.43769562893002
				],
				[
					-26.10904778414117,
					-16.43769562893002
				],
				[
					-23.562949942811347,
					-16.43769562893002
				],
				[
					-21.570163686694098,
					-16.43769562893002
				],
				[
					-19.577377430576846,
					-16.43769562893002
				],
				[
					-17.95202464901483,
					-16.43769562893002
				],
				[
					-16.404481309123327,
					-16.43769562893002
				],
				[
					-14.671059858574488,
					-16.324436751692666
				],
				[
					-13.011125102936674,
					-16.07073686668111
				],
				[
					-11.493840990361536,
					-15.830628046938005
				],
				[
					-9.7258375657369,
					-15.373062182899245
				],
				[
					-8.995293363385855,
					-15.187317624230092
				],
				[
					-8.567341434197985,
					-15.105771232619189
				],
				[
					-8.51114572632483,
					-14.965330224844937
				],
				[
					-8.783478772171685,
					-14.906435608681535
				],
				[
					-10.404508806974219,
					-14.906435608681535
				],
				[
					-10.404508806974219,
					-14.906435608681535
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				7.884591831341595,
				-14.55805543983405
			]
		},
		{
			"id": "D0UI0pAu",
			"type": "text",
			"x": -520.7493980445918,
			"y": -202.75071386696413,
			"width": 351.55673569590624,
			"height": 61.82418199836775,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#69db7c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1778815956,
			"version": 149,
			"versionNonce": 115816556,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1702261111721,
			"link": null,
			"locked": false,
			"text": "Frontend Views",
			"rawText": "Frontend Views",
			"fontSize": 49.459345598694185,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 45.000000000000014,
			"containerId": null,
			"originalText": "Frontend Views",
			"lineHeight": 1.25
		},
		{
			"id": "2wBRnrNw",
			"type": "text",
			"x": 260.5321053254115,
			"y": -776.2489290689142,
			"width": 289.511192281786,
			"height": 44.551194052098054,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#69db7c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 750602988,
			"version": 268,
			"versionNonce": 2143100780,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1702261136562,
			"link": null,
			"locked": false,
			"text": "Ceres Resources",
			"rawText": "Ceres Resources",
			"fontSize": 35.640955241678455,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 31.999999999999986,
			"containerId": null,
			"originalText": "Ceres Resources",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "#69db7c",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 1259.0417769349046,
		"scrollY": 975.1106019101067,
		"zoom": {
			"value": 0.46522602207958597
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%