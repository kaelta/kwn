# Effects of music on living organisms

## Abstract

Within plants, the believed reason for the enhanced growth when subjected to music is due to the vibrations stimulating a specific part called the "protoplasm", which makes the plant grow faster and healthier[^1].
However, there has been a lot of push-back from the scientific community, stating that there were several other uncontrolled factors unaccounted for within these claims, such as the nutrition of the soil, pH levels of the water feed, type of food the plant would consume, uncontrolled ambient noise, etc[^2].

The aim of this study is to investigate and provide more data to existing claims, while also approaching the concept with the novel idea of utilising Chaotic Synthesis to positively affect the growth of the plants. This is in an attempt to provide a level of credence to my theory of utilising the same idea to influence human emotion.
If a suitable rhythm or pattern can be established for plants, it can be then more plausible to work on a human or animal subject.

## Contending studies

### University of Toronto, 2001

The University of Toronto [released a study in 2001](<https://www.esalq.usp.br/lepse/imgs/conteudo_thumb/The-effect-of-sound-on-the-growth-of-plants.pdf>) to gauge the effect of pure sound tones on plants within a controlled environment, however, I believe this study is also flawed as it did not answer the question of *musical* effect, as in, a sound which contains a rhythm, timbre, measure, etc. It also failed to gather conclusive results for many plants that did not grow below 16 days, nor state when the plants had even sprouted[^3].

I find this study flawed for the following reasons:

- The experiment duration was far too short for any substantial results
- The control environment was in a depressing condition
- Large lack of control for ambient noise, any external noise can easily disrupt the musical treatment as the small box would distort sound within the box

### IJIIT, 2015

The International Journal of Integrative Sciences, Innovation and Technology (IJIIT) also conducted a further study, which went further to discuss which specific frequencies aided growth, testing their theory on beans and impatiens plants to 5000 - 14000Hz[^4].
Plants were kept in a chamber similar to the one in the University of Toronto study.

They concluded that Light Indian music made plants grow higher as long as they were given proper care. Plants not given proper care and subjected to the same music did not grow as high, or sprout as many buds.
The same result was also found for the same variation of plant treatments, but for meditation music.
Plants subjected to noise would attempt to bend away from it, and dried far quicker.

I also find this study flawed for the following reasons:

- To not treat a plant as well as one you do is not a suitable control for a study
- The study does not specify what type of noise the plants were subjected to.

## Proposal

To create a system that accounts for the perceived shortcomings of previous studies by heavily regulating the control environment and other conditions the plants grow in, backed up with a large dataset of each control's history that can be easily reviewed by another researcher, or non-technical user.
The study will also measure the effects of controlled chaotic sound synthesis, a multitude of genres on different plants.
The study will be controlled by providing the same level of care to all plants, placed within the same environment, but only remove the music as an external variable.

This will then prove the following:

- A plant can(not) grow as well as another with(out) a musical treatment
- Plants placed within a closed (or open) environment can grow just as well, provided they have been given the same treatment
- Plants have a base understanding of an emotion within music.

[^1]: <http://scienceline.ucsb.edu/getkey.php?key=1495>
[^2]: <https://www.aerifyplants.com/post/does-music-improve-plants-growth>
[^3]: <https://www.esalq.usp.br/lepse/imgs/conteudo_thumb/The-effect-of-sound-on-the-growth-of-plants.pdf>
[^4]: <https://www.researchgate.net/publication/291086163_Effect_of_Music_on_Plants_-_An_Overview>