---

kanban-plugin: basic

---

## Incomplete

- [ ] Validate adding plants only attach to a garden


## In Progress

- [ ] Create the "Add Garden" Modal
- [ ] Basic READ operations through graphql


## Done!

**Complete**
- [x] Fix the UI composition & Hydration errors Next.JS keeps screaming about




%% kanban:settings
```
{"kanban-plugin":"basic"}
```
%%