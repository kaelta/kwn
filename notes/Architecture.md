# Overview of a scale-able plant farming initiative

## Goals

The overall goal of the architecture to Ceres is to support an open network graph of different user's plants, to provide an open API for plant farming data.
The result should provide model training data for the optimal environmental conditions of plants, and note what music each plant responds to best.

## Frontend

The frontend framework chosen is Next.JS, as it provides an extremely suitable API server that can be used for the communication between backend services, as well as serving the GraphQL endpoints needed.
It is powerful, highly flexible, and requires the least amount of work to style, deploy and prototype.

The chosen libraries are:

- **Zod** ~ Schema validation for inputs
- **Jotai** ~ State management library
- **NextUI** ~ CSS framework with an load of built-in components. Saves time styling.
- **Apollo** (Server & Client) ~ Enabling said GraphQL API.
- **Neo4j-Driver** ~ Enabling communication with the backend database.

## Backend

Due to the large amount of fluctuating user data and the graph-like structure of the data, GraphQL and Neo4j have been chosen as the backend services to power the app.
This enables higher flexibility in the storage and manipulation of said data, as well as seek out relations between several types of other records that a relational database is not as suited for in this case.
The final part to the stack is a job queue to run automated tasks, sent to the raspberry pi, which controls various motors and servos responsible for maintaining the variables in the control environment.

The chosen stack is:

- **GraphQL** ~ Communication with gardens and users
- **Neo4j** ~ Holding all recordable and post-able data - gardens, plants, users, flux
- **Kafka** ~ Job runner, scheduling automated tasks for the plants' environmental control devices.
- **Express.JS** ~ Worker API responsible for running Kafka.


## Processing // AI Reporting // Analytics

The main feature of this project is the AI compiling data from the plants and producing automated reports on plant condition, determining trends in growth under different environmental conditions.
This is what is the main draw of Ceres - to yield the best results for each dataset being operated on.
The AI model must be able to be swapped out per use-case, in order to make accurate reporting (We don't want the AI to base its reporting for other data on how well a fern grows!)
The architecture must allow for easily hot-swapping modules in this platform in order to fit the use-case of any other developer/user.

For these reasons, and for future maintainability of this project by other data scientists, AI researchers and the like, the chosen stack is:

- **Flask** ~ Main API communication server.
- **PandasAI** ~ AI modelling library.
- **InfluxDB** ~ Timeless series database, useful for processing large quantities of data at a time.


## Server / Production deployments

The entire stack can be built with [**Nix**](<https://nixos.org>).
The frontend is deployed onto [**Vercel**](<https://vercel.com>), and the backend deployed to [**Fly**](<https://fly.io>).

> We do not need Kubernetes. <https://xeiaso.net/blog/do-i-need-kubernetes/>